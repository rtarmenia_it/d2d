<?php
require '../inc/oracle-db-func.php';

if (is_ajax()) {
    if (isset($_POST["action"]) && !empty($_POST["action"])) { //Checks if action value exists
        $action = $_POST["action"];
        switch($action) { //Switch case for value of action
            case "services": echo fetchAllServices($_POST['contract_id']); break;
            case "new_address": echo setNewAddress($_POST['contract_id'], $_POST['address_id'], $_POST['bnakaran']); break;

        }
    }}



    /*-------------------------------------------------*/
//Function to check if the request is an AJAX request
function is_ajax() {
    return isset($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest';
}