<?php
require '../inc/db-func.php';

if (is_ajax()) {
    if (isset($_POST["action"]) && !empty($_POST["action"])) { //Checks if action value exists
        $action = $_POST["action"];

        $address_id = $_POST['address_id'];

        switch($action) { //Switch case for value of action

            case "house":
                GetShenqData($address_id);
                break;

            case "apt":
                $id = $_POST['apt'];
                GetAptData($address_id, $id);
                break;
        }
    }}
//Function to check if the request is an AJAX request
function is_ajax() {
    return isset($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest';
}