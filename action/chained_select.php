<?php
require '../inc/db-func.php';

if (is_ajax()) {
if (isset($_POST["action"]) && !empty($_POST["action"])) { //Checks if action value exists
$action = $_POST["action"];
switch($action) { //Switch case for value of action
case "city": echo fetchAllCity($_POST['region']); break;
case "district": echo fetchAllDistrict($_POST['city']); break;
case "street": echo fetchAllStreets($_POST['district']); break;
case "street_subtype": echo fetchAllStreetsSubtype($_POST['street'], $_POST['district']); break;
case "house": echo fetchAllHouses($_POST['city'], $_POST['district'], $_POST['street'], $_POST['street_subtype']); break;
}
}}
//Function to check if the request is an AJAX request
function is_ajax() {
return isset($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest';
}