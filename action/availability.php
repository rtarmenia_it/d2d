<?php
require '../inc/oracle-db-func.php';
require '../inc/db-func.php';

if (is_ajax()) {
if (isset($_POST["action"]) && !empty($_POST["action"])) { //Checks if action value exists
$action = $_POST["action"];

$region = $_POST['region'];
$city = trim($_POST['city']);
$district = trim($_POST['district']);
$street = trim($_POST['street']);
$street_subtype = trim($_POST['street_subtype']);
$house = trim($_POST['house']);


if(empty($street_subtype))
{
    $address = '\''.$region.', '.$city.', '.$district.', '.$street.', '.$house.'\'';
}
else{
    $address = '\''.$region.', '.$city.', '.$district.', '.$street.', '.$street_subtype.', '.$house.'\'';
}

//$address = '\''.$region.', '.$city.', '.$district.', '.$street.', '.$house.'\'';

switch($action) { //Switch case for value of action
    case "checkav": echo CheckAvailability($address); break;
    case "checkconn": echo CheckConnectedApp($address); break;
    case "find_building": echo GetAddressID($address); break;
    case "attach":
        $employees = $_POST['employees'];
        $address_id = GetAddressID($address);
        echo AttacheEmployees($address_id, $employees);
        break;
}
}}
//Function to check if the request is an AJAX request
function is_ajax() {
return isset($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest';
}

