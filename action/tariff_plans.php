<?php
require '../inc/oracle-db-func.php';

if (is_ajax()) {
if (isset($_POST["action"]) && !empty($_POST["action"])) { //Checks if action value exists
$action = $_POST["action"];
switch($action) { //Switch case for value of action
case "tv": echo fetchAllIPTvTariffs($_POST['service_name']); break;
case "inet": echo fetchAllInetTariffs($_POST['service_name']); break;
case "trio": echo fetchAllTrios($_POST['service_name']); break;
case "duo": echo fetchAllDuos($_POST['service_name']); break;
case "tel": echo fetchAllTel($_POST['service_name']); break;
case "jamketayin": echo fetchAllJamketayin($_POST['service_name']); break;
case "anjamketayin": echo fetchAllInetTariffs($_POST['service_name']); break;
case "jamketayin_trio": echo fetchAllJamketayinTrios($_POST['service_name']); break;
}
}}
//Function to check if the request is an AJAX request
function is_ajax() {
return isset($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest';
}