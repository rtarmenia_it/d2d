<?php
/**
 * Created by PhpStorm.
 * User: Sevada Ghazaryan
 * Date: 12/2/2015
 * Time: 3:41 PM
 */
require '../inc/oracle-db-func.php';

if (is_ajax()) {
    if (isset($_POST["action"]) && !empty($_POST["action"])) { //Checks if action value exists
        $action = $_POST["action"];
        switch($action) { //Switch case for value of action
            case "partqi_cucak": echo fetchPartqiCucak($_POST['contract_id_list'], $_POST['balance_date']); break;


        }
    }}
//Function to check if the request is an AJAX request
function is_ajax() {
    return isset($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest';
}