<?php
require '../inc/db-func.php';

if (is_ajax()) {
    if (isset($_POST["action"]) && !empty($_POST["action"])) { //Checks if action value exists
        $action = $_POST["action"];

        $address_id = $_POST['address_id'];
        $comments = trim($_POST['comments']);


        switch($action) { //Switch case for value of action

            case "shenq":
                $mutq = trim($_POST['mutq']);
                $hark = trim($_POST['hark']);
                $bnakaran = trim($_POST['bnakaranneri_tiv']);
                $tup = trim($_POST['tup']);
                $address = trim($_POST['address']);

                InsertShenqData($address_id, $address, $mutq, $hark, $bnakaran, $tup, $comments);
                break;

            case "edit-shenq":
                $mutq = trim($_POST['mutq']);
                $hark = trim($_POST['hark']);
                $bnakaran = trim($_POST['bnakaran']);
                $tup = trim($_POST['tup']);

                UpdateShenqData($address_id, $mutq, $hark, $bnakaran, $tup, $comments);
                break;

            case "apt":
                $address = trim($_POST['address']);
                $apt_hamar = trim($_POST['bnakaran']);
                $apt_bnakecvac = trim($_POST['bnakecvac']);
                $inet_operator = trim($_POST['inet_operator']);
                $sak_patet = trim($_POST['sak_patet']);
                $sakagin = trim($_POST['sakagin']);
                $ayl_nshumner = trim($_POST['ayl_nshumner']);
                $contract_end_date = trim($_POST['contract_end']);
                $comments = trim($_POST['comments']);

                InsertAptData($address_id, $address, $apt_hamar, $apt_bnakecvac, $inet_operator, $sak_patet, $sakagin, $contract_end_date, $ayl_nshumner, $comments);
                break;

            case "edit-apt":
                $address_id = trim($_POST['address_id']);
                $apt_hamar = trim($_POST['bnakaran']);
                $old_apt = trim($_POST['old_apt']);
                $apt_bnakecvac = trim($_POST['bnakecvac']);
                $inet_operator = trim($_POST['inet_operator']);
                $sak_patet = trim($_POST['sak_patet']);
                $sakagin = trim($_POST['sakagin']);
                $contract_end_date = trim($_POST['contract_end']);
                $ayl_nshumner = trim($_POST['ayl_nshumner']);
                $comments = trim($_POST['comments']);

                UpdateAptData($address_id, $apt_hamar, $old_apt, $apt_bnakecvac, $inet_operator, $sak_patet, $sakagin, $contract_end_date, $ayl_nshumner, $comments);
                
                break;


            case "delete-apt":
                $address_id = trim($_POST['address_id']);
                $apt_hamar = trim($_POST['bnakaran']);

                DeleteAptData($address_id, $apt_hamar);
                break;

            case "delete-shenq":
                $address_id = trim($_POST['address_id']);

                DeleteShenqData($address_id);
                break;
        }
    }}
//Function to check if the request is an AJAX request
function is_ajax() {
    return isset($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest';
}