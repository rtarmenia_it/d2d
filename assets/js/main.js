'use strict';
var Main = function() {
	var $html = $('html'), $win = $(window), wrap = $('.app-aside'), MEDIAQUERY = {}, app = $('#app');

	MEDIAQUERY = {
		desktopXL: 1200,
		desktop: 992,
		tablet: 768,
		mobile: 480
	};
	$(".current-year").text((new Date).getFullYear());
	//sidebar
	var sidebarHandler = function() {
		var eventObject = isTouch() ? 'click' : 'mouseenter', elem = $('#sidebar'), ul = "", menuTitle, _this;

		elem.on('click', 'a', function(e) {

			_this = $(this);
			if(isSidebarClosed() && !isSmallDevice() && !_this.closest("ul").hasClass("sub-menu"))
				return;

			_this.closest("ul").find(".open").not(".active").children("ul").not(_this.next()).slideUp(200).parent('.open').removeClass("open");
			if(_this.next().is('ul') && _this.parent().toggleClass('open')) {

				_this.next().slideToggle(200, function() {
					$win.trigger("resize");

				});
				e.stopPropagation();
				e.preventDefault();
			} else {
				//_this.parent().addClass("active");

			}
		});
		elem.on(eventObject, 'a', function(e) {
			if(!isSidebarClosed() || isSmallDevice())
				return;
			_this = $(this);

			if(!_this.parent().hasClass('hover') && !_this.closest("ul").hasClass("sub-menu")) {
				wrapLeave();
				_this.parent().addClass('hover');
				menuTitle = _this.find(".item-inner").clone();
				if(_this.parent().hasClass('active')) {
					menuTitle.addClass("active");
				}
				var offset = $("#sidebar").position().top;
				var itemTop = isSidebarFixed() ? _this.parent().position().top + offset : (_this.parent().position().top);
				menuTitle.css({
					position: isSidebarFixed() ? 'fixed' : 'absolute',
					height: _this.outerHeight(),
					top: itemTop
				}).appendTo(wrap);
				if(_this.next().is('ul')) {
					ul = _this.next().clone(true);

					ul.appendTo(wrap).css({
						top: menuTitle.position().top + _this.outerHeight(),
						position: isSidebarFixed() ? 'fixed' : 'absolute',
					});
					if(_this.parent().position().top + _this.outerHeight() + offset + ul.height() > $win.height() && isSidebarFixed()) {
						ul.css('bottom', 0);
					} else {
						ul.css('bottom', 'auto');
					}

					wrap.children().first().scroll(function() {
						if(isSidebarFixed())
							wrapLeave();
					});

					setTimeout(function() {

						if(!wrap.is(':empty')) {
							$(document).on('click tap', wrapLeave);
						}
					}, 300);

				} else {
					ul = "";
					return;
				}

			}
		});
		wrap.on('mouseleave', function(e) {
			$(document).off('click tap', wrapLeave);
			$('.hover', wrap).removeClass('hover');
			$('> .item-inner', wrap).remove();
			$('> ul', wrap).remove();

		});
	};
	// navbar collapse
	var navbarHandler = function() {
		var navbar = $('.navbar-collapse > .nav');
		var pageHeight = $win.innerHeight() - $('header').outerHeight();
		var collapseButton = $('#menu-toggler');
		if(isSmallDevice()) {
			navbar.css({
				height: pageHeight
			});
		} else {
			navbar.css({
				height: 'auto'
			});
		};
		$(document).on("mousedown touchstart", toggleNavbar);
		function toggleNavbar(e) {
			if(navbar.has(e.target).length === 0//checks if descendants of $box was clicked
			&& !navbar.is(e.target)//checks if the $box itself was clicked
			&& navbar.parent().hasClass("collapse in"))  {
				collapseButton.trigger("click");
				//$(document).off("mousedown touchstart", toggleNavbar);
			}
		};
	};
	// tooltips handler
	var tooltipHandler = function() {
		$('[data-toggle="tooltip"]').tooltip();
	};
	// popovers handler
	var popoverHandler = function() {
		$('[data-toggle="popover"]').popover();
	};
	// perfect scrollbar
	var perfectScrollbarHandler = function() {
		var pScroll = $(".perfect-scrollbar");

		if(!isMobile() && pScroll.length) {
			pScroll.perfectScrollbar({
				suppressScrollX: true
			});
			pScroll.on("mousemove", function() {
				$(this).perfectScrollbar('update');
			});

		}
	};
	//toggle class
	var toggleClassOnElement = function() {
		var toggleAttribute = $('*[data-toggle-class]');
		toggleAttribute.each(function() {
			var _this = $(this);
			var toggleClass = _this.attr('data-toggle-class');
			var outsideElement;
			var toggleElement;
			typeof _this.attr('data-toggle-target') !== 'undefined' ? toggleElement = $(_this.attr('data-toggle-target')) : toggleElement = _this;
			_this.on("click", function(e) {
				if(_this.attr('data-toggle-type') !== 'undefined' && _this.attr('data-toggle-type') == "on") {
					toggleElement.addClass(toggleClass);
				} else if(_this.attr('data-toggle-type') !== 'undefined' && _this.attr('data-toggle-type') == "off") {
					toggleElement.removeClass(toggleClass);
				} else {
					toggleElement.toggleClass(toggleClass);
				}
				e.preventDefault();
				if(_this.attr('data-toggle-click-outside')) {

					outsideElement = $(_this.attr('data-toggle-click-outside'));
					$(document).on("mousedown touchstart", toggleOutside);

				};

			});

			var toggleOutside = function(e) {
				if(outsideElement.has(e.target).length === 0//checks if descendants of $box was clicked
				&& !outsideElement.is(e.target)//checks if the $box itself was clicked
				&& !toggleAttribute.is(e.target) && toggleElement.hasClass(toggleClass)) {

					toggleElement.removeClass(toggleClass);
					$(document).off("mousedown touchstart", toggleOutside);
				}
			};

		});
	};
	//switchery
	var switcheryHandler = function() {
		var elems = Array.prototype.slice.call(document.querySelectorAll('.js-switch'));

		elems.forEach(function(html) {
			var switchery = new Switchery(html);
		});
	};
	//search form
	var searchHandler = function() {
		var elem = $('.search-form');
		var searchForm = elem.children('form');
		var formWrap = elem.parent();

		$(".s-open").on('click', function(e) {
			searchForm.prependTo(wrap);
			e.preventDefault();
			$(document).on("mousedown touchstart", closeForm);
		});
		$(".s-remove").on('click', function(e) {
			searchForm.appendTo(elem);
			e.preventDefault();
		});
		var closeForm = function(e) {
			if(!searchForm.is(e.target) && searchForm.has(e.target).length === 0) {
				$(".s-remove").trigger('click');
				$(document).off("mousedown touchstart", closeForm);
			}
		};
	};
	// settings
	var settingsHandler = function() {
		var clipSetting = new Object, appSetting = new Object;
		clipSetting = {
			fixedHeader: true,
			fixedSidebar: true,
			closedSidebar: false,
			fixedFooter: false,
			theme: 'theme-1'
		};
		if($.cookie) {
			if($.cookie("clip-setting")) {
				appSetting = jQuery.parseJSON($.cookie("clip-setting"));
			} else {
				appSetting = clipSetting;
			}
		};

		appSetting.fixedHeader ? app.addClass('app-navbar-fixed') : app.removeClass('app-navbar-fixed');
		appSetting.fixedSidebar ? app.addClass('app-sidebar-fixed') : app.removeClass('app-sidebar-fixed');
		appSetting.closedSidebar ? app.addClass('app-sidebar-closed') : app.removeClass('app-sidebar-closed');
		appSetting.fixedFooter ? app.addClass('app-footer-fixed') : app.removeClass('app-footer-fixed');
		app.hasClass("app-navbar-fixed") ? $('#fixed-header').prop('checked', true) : $('#fixed-header').prop('checked', false);
		app.hasClass("app-sidebar-fixed") ? $('#fixed-sidebar').prop('checked', true) : $('#fixed-sidebar').prop('checked', false);
		app.hasClass("app-sidebar-closed") ? $('#closed-sidebar').prop('checked', true) : $('#closed-sidebar').prop('checked', false);
		app.hasClass("app-footer-fixed") ? $('#fixed-footer').prop('checked', true) : $('#fixed-footer').prop('checked', false);
		$('#skin_color').attr("href", "assets/css/themes/" + appSetting.theme + ".css");
		$('input[name="setting-theme"]').each(function() {
			$(this).val() == appSetting.theme ? $(this).prop('checked', true) : $(this).prop('checked', false);
		});
		switchLogo(appSetting.theme);

		$('input[name="setting-theme"]').change(function() {
			var selectedTheme = $(this).val();
			$('#skin_color').attr("href", "assets/css/themes/" + selectedTheme + ".css");
			switchLogo(selectedTheme);
			appSetting.theme = selectedTheme;
			$.cookie("clip-setting", JSON.stringify(appSetting));

		});

		$('#fixed-header').change(function() {
			$(this).is(":checked") ? app.addClass("app-navbar-fixed") : app.removeClass("app-navbar-fixed");
			appSetting.fixedHeader = $(this).is(":checked");
			$.cookie("clip-setting", JSON.stringify(appSetting));
		});
		$('#fixed-sidebar').change(function() {
			$(this).is(":checked") ? app.addClass("app-sidebar-fixed") : app.removeClass("app-sidebar-fixed");
			appSetting.fixedSidebar = $(this).is(":checked");
			$.cookie("clip-setting", JSON.stringify(appSetting));
		});
		$('#closed-sidebar').change(function() {
			$(this).is(":checked") ? app.addClass("app-sidebar-closed") : app.removeClass("app-sidebar-closed");
			appSetting.closedSidebar = $(this).is(":checked");
			$.cookie("clip-setting", JSON.stringify(appSetting));
		});
		$('#fixed-footer').change(function() {
			$(this).is(":checked") ? app.addClass("app-footer-fixed") : app.removeClass("app-footer-fixed");
			appSetting.fixedFooter = $(this).is(":checked");
			$.cookie("clip-setting", JSON.stringify(appSetting));
		});
		function switchLogo(theme) {
			switch (theme) {
				case "theme-2":
				case "theme-3":
				case "theme-5":
				case "theme-6":
					$(".navbar-brand img").attr("src", "assets/images/logo2.png");
					break;

				default:
					$(".navbar-brand img").attr("src", "assets/images/logo.png");
					break;
			};
		};
		function defaultSetting() {
			$('#fixed-header').prop('checked', true);
			$('#fixed-sidebar').prop('checked', true);
			$('#closed-sidebar').prop('checked', false);
			$('#fixed-footer').prop('checked', false);
			$('#skin_color').attr("href", "assets/css/themes/theme-1.css");
			$(".navbar-brand img").attr("src", "assets/images/logo.png");

		};
	};
	// function to allow a button or a link to open a tab
	var showTabHandler = function(e) {
		if($(".show-tab").length) {
			$('.show-tab').on('click', function(e) {
				e.preventDefault();
				var tabToShow = $(this).attr("href");
				if($(tabToShow).length) {
					$('a[href="' + tabToShow + '"]').tab('show');
				}
			});
		};
	};
	// function to enable panel scroll with perfectScrollbar
	var panelScrollHandler = function() {
		var panelScroll = $(".panel-scroll");
		if(panelScroll.length && !isMobile()) {
			panelScroll.perfectScrollbar({
				suppressScrollX: true
			});
		}
	};
	//function to activate the panel tools
	var panelToolsHandler = function() {

		// panel close
		$('body').on('click', '.panel-close', function(e) {
			var panel = $(this).closest('.panel');

                destroyPanel();

                function destroyPanel() {
                    var col = panel.parent();
                    panel.fadeOut(300, function () {
                        $(this).remove();
                        if (col.is('[class*="col-"]') && col.children('*').length === 0) {
                            col.remove();
                        }
                    });
                }
			e.preventDefault();
		});
		// panel refresh
		$('body').on('click', '.panel-refresh', function(e) {
			var $this = $(this), csspinnerClass = 'csspinner', panel = $this.parents('.panel').eq(0), spinner = $this.data('spinner') || "load1";
			panel.addClass(csspinnerClass + ' ' + spinner);
			
			window.setTimeout(function() {
				panel.removeClass(csspinnerClass);
			}, 1000);
			e.preventDefault();
		});
		// panel collapse
		$('body').on('click', '.panel-collapse', function(e) {
			e.preventDefault();
			var el = $(this);
			var panel = jQuery(this).closest(".panel");
			var bodyPanel = panel.children(".panel-body");
			bodyPanel.slideToggle(200, function() {
				panel.toggleClass("collapses");
			});

		});

	};
	// function to activate the Go-Top button
    var goTopHandler = function(e) {
        $('.go-top').on('click', function(e) {
            $("html, body").animate({
                scrollTop: 0
            }, "slow");
            e.preventDefault();
        });
    };
	var customSelectHandler = function() {
		[].slice.call(document.querySelectorAll('select.cs-select')).forEach(function(el) {
			new SelectFx(el);
		});
	};
	// Window Resize Function
	var resizeHandler = function(func, threshold, execAsap) {
		$(window).resize(function() {
			navbarHandler();
		});
	};
	function wrapLeave() {
		wrap.trigger('mouseleave');
	}

	function isTouch() {
		return $html.hasClass('touch');
	}

	function isSmallDevice() {
		return $win.width() < MEDIAQUERY.desktop;
	}

	function isSidebarClosed() {
		return $('.app-sidebar-closed').length;
	}

	function isSidebarFixed() {
		return $('.app-sidebar-fixed').length;
	}

	function isMobile() {
		if(/Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent)) {
			return true;
		} else {
			return false;
		};
	}

	return {
		init: function() {
			settingsHandler();
			toggleClassOnElement();
			sidebarHandler();
			navbarHandler();
			searchHandler();
			tooltipHandler();
			popoverHandler();
			perfectScrollbarHandler();
			switcheryHandler();
			resizeHandler();
			showTabHandler();
			panelScrollHandler();
			panelToolsHandler();
			customSelectHandler();
			goTopHandler();
		}
	};
}();




$(document).ready(function() {
    var request;
    $(document).on("change", '.state', function(event) {
        var url = 'action/chained_select.php';
        // abort any pending request
        if (request) {
            request.abort();
        }
        // post to the backend script in ajax mode
        var serializedData = 'region='+$(this).find(':selected').val() + '&action=city';
        // fire off the request
        request = $.ajax({
            url: url,
            type: "post",
            data: serializedData
        })
            .done(function (result, textStatus, jqXHR){
                //var resultJSON = processJSONResult(result);
                // Render alerts
                $("select#city").html(result);
                //alert(resultJSON);
            }).fail(function (jqXHR, textStatus, errorThrown){
                // log the error to the console
                console.error(
                    "The following error occured: "+
                    textStatus, errorThrown
                );
            });

        // prevent default posting of form
        event.preventDefault();
    });

});


$(document).ready(function() {
    var request;
    $(document).on("change", '#city', function(event) {
        var url = 'action/chained_select.php';
        // abort any pending request
        if (request) {
            request.abort();
        }
        // post to the backend script in ajax mode
        var serializedData = 'city='+$(this).find(':selected').val() + '&action=district';

        // fire off the request
        request = $.ajax({
            url: url,
            type: "post",
            data: serializedData
        })
            .done(function (result, textStatus, jqXHR){
                //var resultJSON = processJSONResult(result);
                // Render alerts
                $("select#district").html(result);
                //alert(resultJSON);
            }).fail(function (jqXHR, textStatus, errorThrown){
                // log the error to the console
                console.error(
                    "The following error occured: "+
                    textStatus, errorThrown
                );
            });

        // prevent default posting of form
        event.preventDefault();
    });

});

$(document).ready(function() {
    var request;
    $(document).on("change", '#district', function(event) {
        var url = 'action/chained_select.php';
        // abort any pending request
        if (request) {
            request.abort();
        }
        // post to the backend script in ajax mode
        var serializedData = 'district='+$(this).find(':selected').val() + '&action=street';

        // fire off the request
        request = $.ajax({
            url: url,
            type: "post",
            data: serializedData
        })
            .done(function (result, textStatus, jqXHR){
                //var resultJSON = processJSONResult(result);
                // Render alerts
                $("select#street").html(result);
                //alert(resultJSON);
            }).fail(function (jqXHR, textStatus, errorThrown){
                // log the error to the console
                console.error(
                    "The following error occured: "+
                    textStatus, errorThrown
                );
            });

        // prevent default posting of form
        event.preventDefault();
    });

});

$(document).ready(function() {
    var request;
    $(document).on("change", '#street', function(event) {
        var url = 'action/chained_select.php';
        // abort any pending request
        if (request) {
            request.abort();
        }
        // post to the backend script in ajax mode
        var serializedData = 'street='+$(this).find(':selected').val() + '&district='+$('select[name="district"]').val() + '&action=street_subtype';
        var serializedDataCross = 'street_subtype='+'' + '&city='+$('select[name="city"]').val() + '&district='+$('select[name="district"]').val() +
            '&street='+$('select[name="street"]').val()+ '&action=house';

        // fire off the request
        request = $.ajax({
            url: url,
            type: "post",
            datatype: "json",
            data: serializedData
        })
            .done(function (result, textStatus, jqXHR){
                //var resultJSON = processJSONResult(result);
                // Render alerts
                if(result === '<option value="">Ընտրեք նրբանցքը</option><option value="">Նրբանցք չկա</option>'){
                    $("select#street_subtype").html(result);
                    $('[name=street_subtype] option').filter(function () {
                        return ($(this).text() == 'Նրբանցք չկա'); //To select Blue
                    }).prop('selected', true);
                    $("select#street_subtype").attr("disabled", true);
                    request = $.ajax({
                        url: url,
                        type: "post",
                        datatype: "json",
                        data: serializedDataCross
                    })
                        .done(function (result, textStatus, jqXHR){
                            $("select#house").html(result);
                        });
                } else{
                    $("select#street_subtype").attr("disabled", false);
                    $("select#street_subtype").html(result);
                    request = $.ajax({
                        url: url,
                        type: "post",
                        datatype: "json",
                        data: serializedDataCross
                    })
                        .done(function (result, textStatus, jqXHR){
                            $("select#house").html(result);
                        });
                    //$("select#house").empty();
                }
                //alert(resultJSON);
            }).fail(function (jqXHR, textStatus, errorThrown){
                // log the error to the console
                console.error(
                    "The following error occured: "+
                    textStatus, errorThrown
                );
            });

        // prevent default posting of form
        event.preventDefault();
    });

});

$(document).ready(function() {
    var request;
    $(document).on("change", '#street_subtype', function(event) {
        var url = 'action/chained_select.php';
        // abort any pending request
        if (request) {
            request.abort();
        }
        // post to the backend script in ajax mode
        var serializedData = 'street_subtype='+$(this).find(':selected').val() + '&city='+$('select[name="city"]').val() + '&district='+$('select[name="district"]').val() +
            '&street='+$('select[name="street"]').val()+ '&action=house';

        // fire off the request
        request = $.ajax({
            url: url,
            type: "post",
            datatype: "json",
            data: serializedData
        })
            .done(function (result, textStatus, jqXHR){
                //var resultJSON = processJSONResult(result);
                // Render alerts
                $("select#house").html(result);
                //alert(resultJSON);
            }).fail(function (jqXHR, textStatus, errorThrown){
                // log the error to the console
                console.error(
                    "The following error occured: "+
                    textStatus, errorThrown
                );
            });

        // prevent default posting of form
        event.preventDefault();
    });
});

$(document).ready(function() {
    var request;
    $(document).on("click", '#checker', function(event) {
        $('.modal').show();
        var url = 'action/availability.php';
        // abort any pending request
        if (request) {
            request.abort();
        }
           var reg = $('select[name="state"]').val();

        // post to the backend script in ajax mode
        var serializedData = 'region='+reg +
            '&city='+$('select[name="city"]').val() +
            '&district='+$('select[name="district"]').val() +
            '&street='+$('select[name="street"]').val() +
            '&street_subtype='+$('select[name="street_subtype"]').val() +
            '&house='+$('select[name="building"]').val() +
            '&action=checkconn';

        if($('select[name="street_subtype"]').val() === ""){
            var address = reg +', '+$('select[name="city"]').val()+', '+$('select[name="district"]').val()+', '+$('select[name="street"]').val()+', '+$('select[name="building"]').val()+', '+$('input[name="bnakaran"]').val();
        }
        else
        {
            var address = reg +', '+$('select[name="city"]').val()+', '+$('select[name="district"]').val()+', '+$('select[name="street"]').val()+', '+$('select[name="street_subtype"]').val()+', '+$('select[name="building"]').val()+', '+$('input[name="bnakaran"]').val();
        }

        // fire off the request
        request = $.ajax({
            url: url,
            type: "post",
            data: serializedData
        })
            .done(function (result, textStatus, jqXHR){
                $('.modal').hide();
                //alert(result);
                var resultJSON = JSON.parse(result);
                var conn_count = resultJSON.length;
                if(conn_count > 0) {
                    var spans = "";
                    for (var i = 0; i < conn_count; i++) {
                        spans += "<span class='badge'>" + resultJSON[i] + "</span>";
                    }
                    $('#apt_info').remove();
                    $('#connected').prepend("<div class='alert alert-block alert-success fade in' id='apt_info'>" +
                    "<h4 class='alert-heading margin-bottom-10'><i class='ti-check'></i> Նշված բնակարաններում արդեն իսկ օգտվում են մեր ծառայություններից`</h4>" +
                    "<p>" +
                    spans +
                    "</p>" +
                    "</div>");
                }
                else{
                    $('#apt_info').remove();
                    $('#connected').prepend("<div class='alert alert-block alert-danger fade in' id='apt_info'>" +
                    "<h4 class='alert-heading margin-bottom-10'><i class='ti-check'></i> Տվյալ շենքում դեռևս չունենք մեր ծառայություններից օգտվող ակտիվ բաժանորդ</h4>" +
                    "</div>");
                }

            }).fail(function (jqXHR, textStatus, errorThrown){
                // log the error to the console
                console.error(
                    "The following error occured: "+
                    textStatus, errorThrown
                );
            });

        // prevent default posting of form
        event.preventDefault();
    });

});


$(document).ready(function() {
    var request;
    $(document).on("click", '#attach', function(event) {
        $('.modal').show();
        var url = 'action/availability.php';
        // abort any pending request
        if (request) {
            request.abort();
        }
        var reg = $('select[name="state"]').val();

        // post to the backend script in ajax mode
        var serializedData = 'region='+reg +
            '&city='+$('select[name="city"]').val() +
            '&district='+$('select[name="district"]').val() +
            '&street='+$('select[name="street"]').val() +
            '&street_subtype='+$('select[name="street_subtype"]').val() +
            '&house='+$('select[name="building"]').val() +
            '&employees='+$("#select_employees").val() +
            '&action=attach';

        if($('select[name="street_subtype"]').val() === ""){
            var address = reg +', '+$('select[name="city"]').val()+', '+$('select[name="district"]').val()+', '+$('select[name="street"]').val()+', '+$('select[name="building"]').val()+', '+$('input[name="bnakaran"]').val();
        }
        else
        {
            var address = reg +', '+$('select[name="city"]').val()+', '+$('select[name="district"]').val()+', '+$('select[name="street"]').val()+', '+$('select[name="street_subtype"]').val()+', '+$('select[name="building"]').val()+', '+$('input[name="bnakaran"]').val();
        }

        // fire off the request
        request = $.ajax({
            url: url,
            type: "post",
            data: serializedData
        })
            .done(function (result, textStatus, jqXHR){
                $('.modal').hide();
                //alert(result);
                //var resultJSON = JSON.parse(result);
                if(result === 'Նշանակումը կատարված է:') {
                    swal({
                        title: result,
                        type: "success",
                        confirmButtonColor: "#007AFF"
                    });
                    event.preventDefault
                }
                else{
                    swal({
                        title: result,
                        type: "error",
                        confirmButtonColor: "#DD6B55"
                    });
                    event.preventDefault
                }


            }).fail(function (jqXHR, textStatus, errorThrown){
                // log the error to the console
                console.error(
                    "The following error occured: "+
                    textStatus, errorThrown
                );
            });

        // prevent default posting of form
        event.preventDefault();
    });

});










$(document).ready(function() {
    var request;
    $(document).on("click", '#find_building', function(event) {
        $('.modal').show();
        var url = 'action/availability.php';
        // abort any pending request
        if (request) {
            request.abort();
        }
        var reg = $('select[name="state"]').val();

        // post to the backend script in ajax mode
        var serializedData = 'region='+reg +
            '&city='+$('select[name="city"]').val() +
            '&district='+$('select[name="district"]').val() +
            '&street='+$('select[name="street"]').val() +
            '&street_subtype='+$('select[name="street_subtype"]').val() +
            '&house='+$('select[name="building"]').val() +
            '&action=find_building';

        if($('select[name="street_subtype"]').val() === ""){
            var address = reg +', '+$('select[name="city"]').val()+', '+$('select[name="district"]').val()+', '+$('select[name="street"]').val()+', '+$('select[name="building"]').val()+', '+$('input[name="bnakaran"]').val();
        }
        else
        {
            var address = reg +', '+$('select[name="city"]').val()+', '+$('select[name="district"]').val()+', '+$('select[name="street"]').val()+', '+$('select[name="street_subtype"]').val()+', '+$('select[name="building"]').val()+', '+$('input[name="bnakaran"]').val();
        }

        // fire off the request
        request = $.ajax({
            url: url,
            type: "post",
            data: serializedData
        })
            .done(function (result, textStatus, jqXHR){
                $('.modal').hide();
                //alert(result);
                var resultJSON = JSON.parse(result);
                $("#address_id").remove();
                $("#for_address_id").append("<input type='hidden' class='form-control' name='address_id' id='address_id' value=\'"+result+"\'/>");
                $("#submit_apt").prop( "disabled", false );

            }).fail(function (jqXHR, textStatus, errorThrown){
                // log the error to the console
                console.error(
                    "The following error occured: "+
                    textStatus, errorThrown
                );
            });

        // prevent default posting of form
        event.preventDefault();
    });

});









$(document).ready(function() {
    var request;
    $(document).on("click", '#submit_shenq', function(event) {
        $('.modal').show();
        var url = 'action/create_document.php';
        // abort any pending request
        if (request) {
            request.abort();
        }
        var reg = $('select[name="state"]').val();

        // post to the backend script in ajax mode
        var serializedData = 'address_id='+$('input[name="address_id"]').val() +
            '&mutq='+$('input[name="mutq"]').val() +
            '&hark='+$('input[name="hark"]').val() +
            '&bnakaranneri_tiv='+$('input[name="bnakaranneri_tiv"]').val() +
            '&tup='+$('input[name=tup]:checked', '#for-shenq').val() +
            '&comments='+$('textarea[name="comments"]').val() +
            '&action=shenq';

        if($('select[name="street_subtype"]').val() === ""){
            var address = reg +', '+$('select[name="city"]').val()+', '+$('select[name="district"]').val()+', '+$('select[name="street"]').val()+', '+$('select[name="building"]').val();
        }
        else
        {
            var address = reg +', '+$('select[name="city"]').val()+', '+$('select[name="district"]').val()+', '+$('select[name="street"]').val()+', '+$('select[name="street_subtype"]').val()+', '+$('select[name="building"]').val();
        }

        // fire off the request
        request = $.ajax({
            url: url,
            type: "post",
            data: serializedData+'&address='+address
        })
            .done(function (result, textStatus, jqXHR){
                $('.modal').hide();
                if(result === 'Շենքի տվյալներն հաջողությամբ ավելացված են:') {
                    swal({
                        title: result,
                        type: "success",
                        confirmButtonColor: "#007AFF"
                    });
                    event.preventDefault
                }
                else{
                    swal({
                        title: result,
                        type: "error",
                        confirmButtonColor: "#DD6B55"
                    });
                    event.preventDefault
                }

            }).fail(function (jqXHR, textStatus, errorThrown){
                // log the error to the console
                console.error(
                    "The following error occured: "+
                    textStatus, errorThrown
                );
            });

        // prevent default posting of form
        event.preventDefault();
    });

});

$(document).ready(function() {
    var request;
    $(document).on("click", '#submit_shenq_editmode', function(event) {
        $('.modal').show();
        var url = 'action/create_document.php';
        // abort any pending request
        if (request) {
            request.abort();
        }
        // post to the backend script in ajax mode
        var serializedData = 'address_id='+$('input[name="address_id"]').val() +
            '&mutq='+$('input[name="mutq-editmode"]').val() +
            '&hark='+$('input[name="hark-editmode"]').val() +
            '&bnakaran='+$('input[name="bnakaran-editmode"]').val() +
            '&tup='+$('input[name=tup-editmode]:checked', '#for-shenq-editmode').val() +
            '&comments='+$('textarea[name="comments-editmode"]').val() +
            '&action=edit-shenq';

        // fire off the request
        request = $.ajax({
            url: url,
            type: "post",
            data: serializedData
        })
            .done(function (result, textStatus, jqXHR){
                $('.modal').hide();
                if(result === 'Շենքի տվյալներն հաջողությամբ խմբագրված են:') {
                    swal({
                        title: result,
                        type: "success",
                        confirmButtonColor: "#007AFF"
                    });
                    event.preventDefault
                }
                else{
                    swal({
                        title: result,
                        type: "error",
                        confirmButtonColor: "#DD6B55"
                    });
                    event.preventDefault
                }

            }).fail(function (jqXHR, textStatus, errorThrown){
                // log the error to the console
                console.error(
                    "The following error occured: "+
                    textStatus, errorThrown
                );
            });

        // prevent default posting of form
        event.preventDefault();
    });

});



$(document).ready(function() {
    var request;
    $(document).on("click", '#submit_apt', function(event) {
        $('.modal').show();
        var url = 'action/create_document.php';
        // abort any pending request
        if (request) {
            request.abort();
        }
        var reg = $('select[name="state"]').val();

        // post to the backend script in ajax mode
        var serializedData = 'address_id='+$('input[name="address_id"]').val() +
            '&bnakaran='+$('input[name="bnakaran"]').val() +
            '&inet_operator='+$('select[name="inet_operator"]').val() +
            '&sak_patet='+$('input[name="sak_patet"]').val() +
            '&bnakecvac='+$('input[name=bnakecvac]:checked', '#for-apt').val() +
            '&sakagin='+$('input[name="sakagin"]').val() +
            '&contract_end='+$('input[name="contract_end"]').val() +
			'&ayl_nshumner='+$('select[name="ayl_nshumner"]').val() +
            '&comments='+$('textarea[name="comments"]').val() +
            '&action=apt';

        if($('select[name="street_subtype"]').val() === ""){
            var address = reg +', '+$('select[name="city"]').val()+', '+$('select[name="district"]').val()+', '+$('select[name="street"]').val()+', '+$('select[name="building"]').val();//+', '+$('input[name="bnakaran"]').val();
        }
        else
        {
            var address = reg +', '+$('select[name="city"]').val()+', '+$('select[name="district"]').val()+', '+$('select[name="street"]').val()+', '+$('select[name="street_subtype"]').val()+', '+$('select[name="building"]').val();//+', '+$('input[name="bnakaran"]').val();
        }

        // fire off the request
        request = $.ajax({
            url: url,
            type: "post",
            data: serializedData+'&address='+address
        })
            .done(function (result, textStatus, jqXHR){
                $('.modal').hide();
                if(result === 'Բնակարանի տվյալներն հաջողությամբ ավելացված են:') {
                    swal({
                        title: result,
                        type: "success",
                        confirmButtonColor: "#007AFF"
                    },
                        function (isConfirm) {
                            if (isConfirm) {
                                window.location = window.location.pathname;
                                window.location.reload(true);
                            }
                        });

                    event.preventDefault

                }

                else{
                    swal({
                        title: result,
                        type: "error",
                        confirmButtonColor: "#DD6B55"
                    });
                    event.preventDefault
                }

            }).fail(function (jqXHR, textStatus, errorThrown){
                // log the error to the console
                console.error(
                    "The following error occured: "+
                    textStatus, errorThrown
                );
            });

        // prevent default posting of form
        event.preventDefault();
    });

});



$(document).ready(function() {
    var request;
    $(document).on("click", '#submit_apt_editmode', function(event) {
        $('.modal').show();
        var url = 'action/create_document.php';
        // abort any pending request
        if (request) {
            request.abort();
        }

        // post to the backend script in ajax mode
        var serializedData = 'address_id='+$('input[name="address_id"]').val() +
            '&bnakaran='+$('input[name="bnakaran-editmode"]').val() +
            '&old_apt='+$('input[name="old_apt"]').val() +
            '&inet_operator='+$('select[name="inet_operator-editmode"]').val() +
            '&sak_patet='+$('input[name="sak_patet-editmode"]').val() +
            '&bnakecvac='+$('input[name=bnakecvac-editmode]:checked', '#for-apt-editmode').val() +
            '&sakagin='+$('input[name="sakagin-editmode"]').val() +
            '&contract_end='+$('input[name="contract_end-editmode"]').val() +
            '&ayl_nshumner='+$('select[name="ayl_nshumner-editmode"]').val() +
            '&comments='+$('textarea[name="comments-editmode"]').val() +
            '&action=edit-apt';


        // fire off the request
        request = $.ajax({
            url: url,
            type: "post",
            data: serializedData
        })
            .done(function (result, textStatus, jqXHR){
                $('.modal').hide();
                if(result === 'Բնակարանի տվյալներն հաջողությամբ խմբագրված են:') {
                    swal({
                        title: result,
                        type: "success",
                        confirmButtonColor: "#007AFF"
                    });
                    event.preventDefault
                }
                else{
                    swal({
                        title: result,
                        type: "error",
                        confirmButtonColor: "#DD6B55"
                    });
                    event.preventDefault
                }

            }).fail(function (jqXHR, textStatus, errorThrown){
                // log the error to the console
                console.error(
                    "The following error occured: "+
                    textStatus, errorThrown
                );
            });

        // prevent default posting of form
        event.preventDefault();
    });

});



/*delete row*/
$(document).ready(function() {
    var request;
    $(document).on("click", '#delete_apt', function(event) {
        $('.modal').show();
        var url = 'action/create_document.php';
        // abort any pending request
        if (request) {
            request.abort();
        }

        // post to the backend script in ajax mode
        var serializedData = 'address_id='+$('input[name="address_id"]').val() +
            '&bnakaran='+$('input[name="bnakaran-editmode"]').val() +
            '&action=delete-apt';


        // fire off the request
        request = $.ajax({
            url: url,
            type: "post",
            data: serializedData
        })
            .done(function (result, textStatus, jqXHR){
                $('.modal').hide();
                if(result === 'Բնակարանի տվյալներն հաջողությամբ հեռացված են:') {
                    swal({
                        title: result,
                        type: "success",
                        confirmButtonColor: "#007AFF"
                    },

                        function (isConfirm) {
                            if (isConfirm) {
                                window.location = window.location.pathname;
                                window.location.reload(true);
                            }
                            }
                    );

                    event.preventDefault
                }
                else{
                    swal({
                        title: result,
                        type: "error",
                        confirmButtonColor: "#DD6B55"
                    });
                    event.preventDefault
                }

            }).fail(function (jqXHR, textStatus, errorThrown){
                // log the error to the console
                console.error(
                    "The following error occured: "+
                    textStatus, errorThrown
                );
            });

        // prevent default posting of form
        event.preventDefault();
    });

});



/*delete shenq*/
$(document).ready(function() {
    var request;
    $(document).on("click", '#delete_shenq', function(event) {
        $('.modal').show();
        var url = 'action/create_document.php';
        // abort any pending request
        if (request) {
            request.abort();
        }

        // post to the backend script in ajax mode
        var serializedData = 'address_id='+$('input[name="address_id"]').val() +
            '&action=delete-shenq';


        // fire off the request
        request = $.ajax({
            url: url,
            type: "post",
            data: serializedData
        })
            .done(function (result, textStatus, jqXHR){
                $('.modal').hide();
                if(result === 'Շենքի տվյալներն հաջողությամբ հեռացված են:') {
                    swal({
                            title: result,
                            type: "success",
                            confirmButtonColor: "#007AFF"
                        },

                        function (isConfirm) {
                            if (isConfirm) {
                                window.location = window.location.pathname;
                                window.location.reload(true);
                            }
                        }
                    );

                    event.preventDefault
                }
                else{
                    swal({
                        title: result,
                        type: "error",
                        confirmButtonColor: "#DD6B55"
                    });
                    event.preventDefault
                }

            }).fail(function (jqXHR, textStatus, errorThrown){
                // log the error to the console
                console.error(
                    "The following error occured: "+
                    textStatus, errorThrown
                );
            });

        // prevent default posting of form
        event.preventDefault();
    });

});











$(document).ready(function() {
    var request;
    $(document).on("click", '#edit-shenq', function(event) {
        var url = 'action/getData.php';
        // abort any pending request
        if (request) {
            request.abort();
        }
        // post to the backend script in ajax mode
        var serializedData = 'address_id='+$(this).data('id') + '&action=house';

        // fire off the request
        request = $.ajax({
            url: url,
            type: "post",
            datatype: "json",
            data: serializedData
        })
            .done(function (result, textStatus, jqXHR){
                var resultJSON = processJSONResult(result);
                // Render alerts
                //$("select#house").html(result);
                //alert(resultJSON[0]['harkeri_tiv']);
                $("#for-shenq-editmode").html("<div class='col-sm-6'>" +
                "<div class='form-group'>" +
                "<fieldset><legend>Շենքի տվյալներ</legend>" +
                "<div class='row'>" +
                "<div class='col-md-6'>" +
                "<div class='form-group'>" +
                "<label>" +
                "Մուտքերի քանակ <span class='symbol required'></span>" +
                "</label><input type='text' placeholder='Մուտքերի քանակ' class='form-control' id='mutq-editmode' name='mutq-editmode' value='"+resultJSON[0]['mutqeri_qanak']+"'/></div></div>" +
                "<div class='col-md-6'><div class='form-group'><label class='control-label'>Հարկերի թիվ <span class='symbol required'></span></label>" +
                "<input type='text' placeholder='Հարկերի թիվ' class='form-control' id='hark-editmode' name='hark-editmode' value='"+resultJSON[0]['harkeri_tiv']+"'/></div></div><div class='col-md-6'>" +
                "<div class='form-group'><label class='control-label'>Բնակարանների թիվ <span class='symbol required'></span></label>" +
                "<input type='text' placeholder='Բնակարանների թիվ' class='form-control' id='bnakran-editmode' name='bnakaran-editmode' value='"+resultJSON[0]['bnakaranneri_tiv']+"'/></div></div>" +
                "<div class='col-md-6'><div class='form-group'><label class='block'>Տուփի առկայություն&nbsp<span class='label label-info'>"+resultJSON[0]['tup']+"</span></label><div class='clip-radio radio-primary'>" +
                "<input type='radio' id='tup-ka-editmode' name='tup-editmode' value='Կա'><label for='tup-ka-editmode'>Կա</label>" +
                "<input type='radio' id='tup-chka-editmode' name='tup-editmode' value='Չկա'><label for='tup-chka-editmode'>Չկա</label></div></div></div></div>" +
                "<div class='row'><div class='col-md-sm-6'><div class='form-group'><div class='panel panel-white'><div class='panel-heading'><div class='panel-title'>Այլ մեկնաբանություններ</div>" +
                "</div><div class='panel-body'><div class='form-group'><div class='note-editor'><textarea class='form-control' id='comments-editmode' name='comments-editmode'>"+resultJSON[0]['comments']+"</textarea>" +
                "</div></div></div></div></div></div></div>" +
					"<button class='btn btn-success btn-block' id='submit_shenq_editmode'>Խմբագրել</button>" +
                    "<button class='btn btn-danger btn-block' id='delete_shenq'>Delete</button>" +
					"</fieldset></div>" +
                "<input type='hidden' name='address_id' id='address_id' value='"+resultJSON[0]['address_id']+"'></div>");
                if(resultJSON[0]['tup'] === 'Կա'){
                    $("#tup-ka-editmode").prop("checked", true);
                }
                else{
                    $("#tup-chka-editmode").prop("checked", true);
                }
            }).fail(function (jqXHR, textStatus, errorThrown){
                // log the error to the console
                console.error(
                    "The following error occured: "+
                    textStatus, errorThrown
                );
            });

        // prevent default posting of form
        event.preventDefault();
    });
});







$(document).ready(function() {
    var request;
    $(document).on("click", '#edit-apt', function(event) {
        var url = 'action/getData.php';
        // abort any pending request
        if (request) {
            request.abort();
        }
        // post to the backend script in ajax mode
        var serializedData = 'address_id='+$(this).data('id') + '&apt='+$(this).data('uid') + '&action=apt';

        // fire off the request
        request = $.ajax({
            url: url,
            type: "post",
            datatype: "json",
            data: serializedData
        })
            .done(function (result, textStatus, jqXHR){
                var resultJSON = processJSONResult(result);
                // Render alerts
                //$("select#house").html(result);
                //alert(resultJSON[0]['harkeri_tiv']);
                $("#for-apt-editmode").html("<div class='col-sm-6'><div class='form-group'><fieldset><legend>Բնակարանի տվյալներ</legend>" +
                "<div class='row'><div class='col-md-6'><div class='form-group'><label>Բնակարանի համար <span class='symbol required'></span></label>" +
                "<input type='text' placeholder='Բնակարանի համար' class='form-control' id='bnakaran-editmode' name='bnakaran-editmode' value='"+resultJSON[0]['apt_hamar']+"'/></div></div>" +
                "<div class='col-md-6'><div class='form-group'><label class='block'>Բնակեցված է</label><div class='clip-radio radio-primary'>" +
                "<input type='radio' id='bnak-yes-editmode' name='bnakecvac-editmode' value='Այո'><label for='bnak-yes-editmode'>Այո</label>" +
                "<input type='radio' id='bnak-no-editmode' name='bnakecvac-editmode' value='Ոչ'><label for='bnak-no-editmode'>Ոչ</label></div></div></div>" +
                "<div class='col-md-6'><div class='form-group'><label class='control-label'>Ինտերնետ օպերատորի անունը <span class=''></span></label>" +
					"<select id='inet_operator-editmode' name='inet_operator-editmode' class='js-example-placeholder-operator js-states form-control'>" +
						"<option selected value='"+resultJSON[0]['inet_operator']+"'>"+resultJSON[0]['inet_operator']+"</option>"+
						"<option value='Ucom'>Ucom</option>" +
						"<option value='Beeline'>Beeline</option>" +
						"<option value='Vivacell'>VivaCell</option>" +
						"<option value='Orange'>Orange</option>" +
						"<option value='Interactive'>Interactive</option>" +
						"<option value='Այլ'>Այլ տարբերակ</option>" +
					"</select>" +
				"</div></div>" +
                "<div class='col-md-6'><div class='form-group'><label class='control-label'>Սակագնային փաթեթի անվանումը <span class=''></span></label>" +
                "<input type='text' placeholder='Սակագնային փաթեթի անվանումը' class='form-control' id='sak_patet-editmode' name='sak_patet-editmode' value='"+resultJSON[0]['sak_patet']+"'/></div></div>" +
                "<div class='col-md-6'><div class='form-group'><label class='control-label'>Սակագին <span class=''></span></label>" +
                "<input type='text' placeholder='Սակագին' class='form-control' id='sakagin-editmode' name='sakagin-editmode' value='"+resultJSON[0]['sakagin']+"'/></div></div>" +
                "<div class='col-md-6'><div class='form-group'><label class='control-label'>Պայմանագրի ավարտը <span class=''></span></label>" +
                "<input type='text' placeholder='Պայմանագրի ավարտը' class='form-control' id='contract_end-editmode' name='contract_end-editmode' value='"+resultJSON[0]['contract_end_date']+"'/></div></div>" +
					"<div class='col-md-12'><div class='form-group'><label class='control-label'>Այլ նշումներ <span class=''></span></label>" +
                "<select id='ayl_nshumner-editmode' name='ayl_nshumner-editmode' class='js-example-placeholder-operator js-states form-control'>" +
                "<option selected value='"+resultJSON[0]['ayl_nshumner']+"'>"+resultJSON[0]['ayl_nshumner']+"</option>"+
                "<option value='Տարեցներ'>Տարեցներ</option>"+
                "<option value='Դուռը_չբացեցին'>Դուռը չբացեցին</option>"+
                "<option value='Չեն_բնակվում'>Չեն բնակվում</option>"+
                "<option value='Վճարունակ_չեն'>Վճարունակ չեն</option>"+
                "<option value='Ինտերնետից_չեն_օգտվում'>Ինտերնետից չեն օգտվում</option>"+
                "</select>"+
                "</div></div></div>" +
				"<div class='row'><div class='col-md-sm-6'><div class='form-group'><div class='panel panel-white'><div class='panel-heading'>" +
                "<div class='panel-title'>Այլ մեկնաբանություններ</div></div><div class='panel-body'><div class='form-group'>" +
                "<div class='note-editor'><textarea class='form-control' id='comments-editmode' name='comments-editmode'>"+resultJSON[0]['apt_comments']+"</textarea>" +
                "</div></div></div></div></div></div></div>" +
					"<button class='btn btn-success btn-block' id='submit_apt_editmode'>Հաստատել</button>" +
                    "<button class='btn btn-danger btn-block' id='delete_apt'>Delete</button>" +
					"</fieldset></div></div>" +
                "<input type='hidden' name='address_id' id='address_id' value='"+resultJSON[0]['address_id']+"'></div>" +
                "<input type='hidden' name='old_apt' id='old_apt' value='"+resultJSON[0]['apt_hamar']+"'></div>");
                if(resultJSON[0]['apt_bnakecvac'] === 'Այո'){
                    $("#bnak-yes-editmode").prop("checked", true);
                }
                else{
                    $("#bnak-no-editmode").prop("checked", true);
                }
            }).fail(function (jqXHR, textStatus, errorThrown){
                // log the error to the console
                console.error(
                    "The following error occured: "+
                    textStatus, errorThrown
                );
            });

        // prevent default posting of form
        event.preventDefault();
    });
});




$(document).ready(function() {
    var request;
    $(document).on("click", '#submit_contract', function(event) {
        var url = 'action/addressCorrection.php';
        // abort any pending request
        if (request) {
            request.abort();
        }
        // post to the backend script in ajax mode
        var serializedData = 'contract_id='+$('#contract_id').val() + '&action=services';

        // fire off the request
        request = $.ajax({
            url: url,
            type: "post",
            datatype: "json",
            data: serializedData
        })
            .done(function (result, textStatus, jqXHR){
                //var resultJSON = processJSONResult(result);
                // Render alerts
                //$("select#house").html(result);
                //alert(resultJSON[0]['harkeri_tiv']);
                $("#for-services").html(result);

            }).fail(function (jqXHR, textStatus, errorThrown){
                // log the error to the console
                console.error(
                    "The following error occured: "+
                    textStatus, errorThrown
                );
            });

        // prevent default posting of form
        event.preventDefault();
    });
});



$(document).ready(function() {
    var request;
    $(document).on("click", '#find_building_address_correction', function(event) {
        $('.modal').show();
        var url = 'action/availability.php';
        // abort any pending request
        if (request) {
            request.abort();
        }
        var reg = $('select[name="state"]').val();

        // post to the backend script in ajax mode
        var serializedData = 'region='+reg +
            '&city='+$('select[name="city"]').val() +
            '&district='+$('select[name="district"]').val() +
            '&street='+$('select[name="street"]').val() +
            '&street_subtype='+$('select[name="street_subtype"]').val() +
            '&house='+$('select[name="building"]').val() +
            '&action=find_building';

        if($('select[name="street_subtype"]').val() === ""){
            var address = reg +', '+$('select[name="city"]').val()+', '+$('select[name="district"]').val()+', '+$('select[name="street"]').val()+', '+$('select[name="building"]').val()+', '+$('input[name="bnakaran"]').val();
        }
        else
        {
            var address = reg +', '+$('select[name="city"]').val()+', '+$('select[name="district"]').val()+', '+$('select[name="street"]').val()+', '+$('select[name="street_subtype"]').val()+', '+$('select[name="building"]').val()+', '+$('input[name="bnakaran"]').val();
        }

        // fire off the request
        request = $.ajax({
            url: url,
            type: "post",
            data: serializedData
        })
            .done(function (result, textStatus, jqXHR){
                $('.modal').hide();
                //alert(result);
                var resultJSON = JSON.parse(result);
                $("#address_id").remove();
                $("#for_address_id").append("<input type='hidden' class='form-control' name='address_id' id='address_id' value=\'"+result+"\'/>");
                $("#new_address_id").val(result);

            }).fail(function (jqXHR, textStatus, errorThrown){
                // log the error to the console
                console.error(
                    "The following error occured: "+
                    textStatus, errorThrown
                );
            });

        // prevent default posting of form
        event.preventDefault();
    });

});


$(document).ready(function() {
    var request;
    $(document).on("click", '#submit_new_address', function(event) {
        $('.modal').show();
        var url = 'action/addressCorrection.php';
        // abort any pending request
        if (request) {
            request.abort();
        }

        // post to the backend script in ajax mode
        var serializedData = 'address_id='+$('input[name="new_address_id"]').val() +
            '&contract_id='+$('input[name="address_correction_contract_id"]').val() +
            '&bnakaran='+$('input[name="address_correction_bnakaran"]').val() +
            '&action=new_address';


        // fire off the request
        request = $.ajax({
            url: url,
            type: "post",
            data: serializedData
        })
            .done(function (result, textStatus, jqXHR){
                $('.modal').hide();
                if(result === 'Հասցեն ուղղված է։') {
                    swal({
                        title: result,
                        type: "success",
                        confirmButtonColor: "#007AFF"
                    });
                    event.preventDefault
                }
                else{
                    swal({
                        title: result,
                        type: "error",
                        confirmButtonColor: "#DD6B55"
                    });
                    event.preventDefault
                }

            }).fail(function (jqXHR, textStatus, errorThrown){
                // log the error to the console
                console.error(
                    "The following error occured: "+
                    textStatus, errorThrown
                );
            });

        // prevent default posting of form
        event.preventDefault();
    });

});



$(document).ready(function() {
	var request;
	$(document).on("click", '#submit_partq', function(event) {
		var url = 'action/partq.php';
		// abort any pending request
		if (request) {
			request.abort();
		}
		// post to the backend script in ajax mode
		var serializedData = 'contract_id_list='+$('#contract_list').val() + '&balance_date='+$('#balance_date').val() + '&action=partqi_cucak';

		// fire off the request
		request = $.ajax({
					url: url,
					type: "post",
					datatype: "json",
					data: serializedData
				})
				.done(function (result, textStatus, jqXHR){
					//var resultJSON = processJSONResult(result);
					// Render alerts
					//$("select#house").html(result);
					//alert(resultJSON[0]['harkeri_tiv']);
					//$("#for-services").html(result);
					//window.location="download.php?filename=export.csv";

					$("#partqi-cucak").html(result);
					$('#example').DataTable( {
						dom: 'Bfrtip',
						buttons: [
							'copy', 'csv', 'excel', 'pdf', 'print'
						],
						"columnDefs": [
							{
								"targets": [ 5 ],
								"visible": false,
								"searchable": false
							},
							{
								"targets": [ 10 ],
								"visible": false
							},
							{
								"targets": [ 15 ],
								"visible": false
							}
						]
					} );

				}).fail(function (jqXHR, textStatus, errorThrown){
					// log the error to the console
					console.error(
							"The following error occured: "+
							textStatus, errorThrown
					);
				});

		// prevent default posting of form
		event.preventDefault();
	});
});












function processJSONResult(result) {
    if (!result) {
        addAlert("danger", "Oops, this feature doesn't seem to be working at the moment.  Totally our bad.");
        return {"errors": 1, "successes": 0};
    } else {
        try {
            if (typeof result == 'string') {
                return jQuery.parseJSON(result);
            } else {
                return result;
            }
        } catch (err) {
            console.log("Backend error: " + result);
            addAlert("danger", "Oops, looks like our server might have goofed.  If you're an admin, please check the PHP error logs.");
            return {"errors": 1, "successes": 0};
        }
    }
}