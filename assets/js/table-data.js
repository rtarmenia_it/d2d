var TableData = function() {
	"use strict";
	//function to initiate DataTable
	//DataTable is a highly flexible tool, based upon the foundations of progressive enhancement,
	//which will add advanced interaction controls to any HTML table
	//For more information, please visit https://datatables.net/
	var runDataTable_example1 = function() {

        var oTable = $('#sample_1').dataTable({

            "dom": 'Bfrtip',
            "buttons": [
                'copyHtml5',
                'excelHtml5',
                'csvHtml5'
            ],

            "processing": true,
            "serverSide": true,
            "ajax": "inc/fetchAllShenq.php",
            "aoColumnDefs" : [{
                "aTargets" : [0]
            }],
            "oLanguage" : {
                "sLengthMenu" : "Ցուցադրել _MENU_ տող",
                "sSearch" : "",
                "oPaginate" : {
                    "sPrevious" : "Նախորդ",
                    "sNext" : "Հաջորդ"
                }
            },
            "aaSorting" : [[1, 'asc']],
            "aLengthMenu" : [[5, 10, 15, 20, -1], [5, 10, 15, 20, "All"] // change per page values here
            ],
            // set the initial value
            "iDisplayLength" : 10,
        });
        $('#sample_1_wrapper .dataTables_filter input').addClass("form-control input-sm").attr("placeholder", "Որոնել");
        // modify table search input
        $('#sample_1_wrapper .dataTables_length select').addClass("m-wrap small");
        // modify table per page dropdown
        $('#sample_1_wrapper .dataTables_length select').select2();
        // initialzie select2 dropdown
        $('#sample_1_column_toggler input[type="checkbox"]').change(function() {
            /* Get the DataTables object again - this is not a recreation, just a get of the object */
            var iCol = parseInt($(this).attr("data-column"));
            var bVis = oTable.fnSettings().aoColumns[iCol].bVisible;
            oTable.fnSetColumnVis(iCol, ( bVis ? false : true));

        });

    };

    var runDataTable_example2 = function() {

        var oTable_2 = $('#sample_2').dataTable({

            "dom": 'Bfrtip',
            "buttons": [
                'copyHtml5',
                'excelHtml5',
                'csvHtml5'
            ],

            "processing": true,
            "serverSide": true,
            "ajax": "inc/fetchAllApt.php",
            "aoColumnDefs" : [{
                "aTargets" : [0]
            }],
            "oLanguage" : {
                "sLengthMenu" : "Ցուցադրել _MENU_ տող",
                "sSearch" : "",
                "oPaginate" : {
                    "sPrevious" : "",
                    "sNext" : ""
                }
            },
            "aaSorting" : [[1, 'asc']],
            "aLengthMenu" : [[5, 10, 15, 20, -1], [5, 10, 15, 20, "All"] // change per page values here
            ],
            // set the initial value
            "iDisplayLength" : 10,
        });
        $('#sample_2_wrapper .dataTables_filter input').addClass("form-control input-sm").attr("placeholder", "Որոնել");
        // modify table search input
        $('#sample_2_wrapper .dataTables_length select').addClass("m-wrap small");
        // modify table per page dropdown
        $('#sample_2_wrapper .dataTables_length select').select2();
        // initialzie select2 dropdown
        $('#sample_2_column_toggler input[type="checkbox"]').change(function() {
            /* Get the DataTables object again - this is not a recreation, just a get of the object */
            var iCol = parseInt($(this).attr("data-column"));
            var bVis = oTable_2.fnSettings().aoColumns[iCol].bVisible;
            oTable_2.fnSetColumnVis(iCol, ( bVis ? false : true));
        });
    };
	return {
		//main function to initiate template pages
		init : function() {
			runDataTable_example1();
            runDataTable_example2();
		}
	};


}();


/*$(document).ready(function() {

    $('#sample_1').api().columns().every( function () {
                var column = this;
                var select = $('<select><option value=""></option></select>')
                    .appendTo( $(column.footer()).empty() )
                    .on( 'change', function () {
                        var val = $.fn.dataTable.util.escapeRegex(
                            $(this).val()
                        );

                        column
                            .search( val ? '^'+val+'$' : '', true, false )
                            .draw();
                    } );

                column.data().unique().sort().each( function ( d, j ) {
                    select.append( '<option value="'+d+'">'+d+'</option>' )
                } );
            } );

} );*/

