<?php include_once('classes/check.class.php'); ?>
<?php include_once('inc/db-func.php'); ?>
<?php if( protectThis("1, 3") ) : ?>
<!DOCTYPE html>
<!--[if IE 8]><html class="ie8" lang="en"><![endif]-->
<!--[if IE 9]><html class="ie9" lang="en"><![endif]-->
<!--[if !IE]><!-->
<html lang="en">
	<!--<![endif]-->
	<!-- start: HEAD -->
    <?php include "templates/header.php" ?>
	<body>
    <div class="modal"></div>
		<div id="app">
        <?php include "templates/sidebar.php" ?>

			<div class="app-content">
				<!-- start: TOP NAVBAR -->
                <?php include "templates/header-navbar.php" ?>
				<div class="main-content" >
					<div class="wrap-content container" id="container">
						<!-- start: BASIC MAP -->
						<div class="container-fluid container-fullw bg-white">
							<div class="row">
								<div class="col-sm-12">
									<h5 class="over-title margin-bottom-15">Երևան <span class="text-bold">քարտեզ</span></h5>
									<div class="map" id="map1"></div>
								</div>
							</div>
						</div>
						<!-- end: BASIC MAP -->
                        <!-- start: SELECT BOXES -->
                        <div class="container-fluid container-fullw bg-white">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="row">
                                        <form>
                                            <div class="col-sm-6">
                                                <div class="panel panel-transparent">
                                                    <div class="panel-body">
                                                        <div class="form-group">
                                                            <select class="js-example-placeholder-single js-states form-control state" name="state">
                                                                <option></option>
                                                                <option value="Երևան" data-id=Երևան>Երևան</option>
                                                                <option value="Լոռի" data-id=Լոռի>Լոռի</option>
                                                                <option value="Սյունիք" data-id=Սյունիք>Սյունիք</option>
                                                                <option value="Արագածոտն" data-id=Արագածոտն>Արագածոտն</option>
                                                                <option value="Կոտայք" data-id=Կոտայք>Կոտայք</option>
                                                                <option value="Արարատ" data-id=Արարատ>Արարատ</option>
                                                                <option value="Արմավիր" data-id=Արմավիր>Արմավիր</option>
                                                                <option value="Տավուշ" data-id=Տավուշ>Տավուշ</option>
                                                                <option value="Գեղարքունիք" data-id=Գեղարքունիք>Գեղարքունիք</option>
                                                                <option value="Շիրակ" data-id=Շիրակ>Շիրակ</option>
                                                                <option value="Վայոց Ձոր" data-id=Վայոց Ձոր>Վայոց Ձոր</option>
                                                            </select>
                                                            </div>
                                                                <div class="form-group">
                                                            <select class="js-example-placeholder-city js-states form-control" name="city" class="city"  id="city">
                                                                <option></option>
                                                            </select>
                                                                </div>
                                                                <div class="form-group">
                                                            <select class="js-example-placeholder-district js-states form-control" name="district" class="district" id="district">
                                                                <option></option>
                                                            </select>
                                                                    </div>
                                                                <div class="form-group">
                                                            <select class="js-example-placeholder-street js-states form-control" name="street" class="street" id="street">
                                                                <option></option>
                                                            </select>
                                                                    </div>
                                                            <div class="form-group">
                                                            <select class="js-example-placeholder-street_subtype js-states form-control" name="street_subtype" class="street_subtype" id="street_subtype">
                                                                <option></option>
                                                            </select>
                                                                </div>
                                                            <div class="form-group">
                                                            <select class="js-example-placeholder-house js-states form-control" name="building" class="building" id="house">
                                                                <option></option>
                                                            </select>
                                                                </div>
                                                            <div class="form-group">
                                                                <button class="btn btn-primary btn-block" id="checker">Փնտրել</button>
                                                            </div>

                                                        </div>
                                                    </div>
                                                </div>
                                            <div class="col-sm-6" id="connected">
                                            <div class="row">
                                            <div class="col-sm-12">
                                                <h5 class="over-title">Նշանակել</h5>
                                                <p>
                                                    Նշանակել կոնկրետ աշխատակիցների տվյալ հասցեի համար
                                                </p>
                                            </div>
                                            <form>
                                            <div class="col-sm-12">
                                            <div class="panel panel-transparent">
                                            <div class="panel-body">

                                                <div class="form-group">
                                                    <label>
                                                        Կարող եք ընտրել մի քանի հոգու
                                                    </label>

                                                    <select multiple="" name="select_employees[]" id="select_employees" class="js-example-basic-multiple js-states form-control">
                                                        <?php echo GetAllEmployees(); ?>
                                                    </select>
                                                </div>
                                                <div class="form-group">
                                                    <button class="btn btn-success btn-block" id="attach">Նշանակել</button>
                                                </div>

                                            </div>
                                            </div>
                                            </div>

                                            </form>
                                            </div>

                                            </div>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- end: SELECT BOXES -->
					</div>
				</div>
			</div>
			<!-- start: FOOTER -->
            <?php include "templates/footer.php" ?>
			<!-- end: FOOTER -->
		</div>
		<!-- start: MAIN JAVASCRIPTS -->
		<script src="vendor/jquery/jquery.min.js"></script>
		<script src="vendor/bootstrap/js/bootstrap.min.js"></script>
		<script src="vendor/modernizr/modernizr.js"></script>
		<script src="vendor/jquery-cookie/jquery.cookie.js"></script>
		<script src="vendor/perfect-scrollbar/perfect-scrollbar.min.js"></script>
		<script src="vendor/switchery/switchery.min.js"></script>
		<!-- end: MAIN JAVASCRIPTS -->
		<!-- start: JAVASCRIPTS REQUIRED FOR THIS PAGE ONLY -->
		<script type="text/javascript" src="http://maps.google.com/maps/api/js?sensor=true"></script>
		<script src="vendor/gmaps/gmaps.js"></script>
        <!-- start: JAVASCRIPTS REQUIRED FOR THIS PAGE ONLY -->
        <script src="vendor/maskedinput/jquery.maskedinput.min.js"></script>
        <script src="vendor/bootstrap-touchspin/jquery.bootstrap-touchspin.min.js"></script>
        <script src="vendor/autosize/autosize.min.js"></script>
        <script src="vendor/selectFx/classie.js"></script>
        <script src="vendor/selectFx/selectFx.js"></script>
        <script src="vendor/select2/select2.min.js"></script>
        <script src="vendor/bootstrap-datepicker/bootstrap-datepicker.min.js"></script>
        <script src="vendor/bootstrap-timepicker/bootstrap-timepicker.min.js"></script>
        <!-- end: JAVASCRIPTS REQUIRED FOR THIS PAGE ONLY -->
    <!-- start: JAVASCRIPTS REQUIRED FOR THIS PAGE ONLY -->
    <script src="vendor/sweetalert/sweet-alert.min.js"></script>
    <script src="vendor/toastr/toastr.min.js"></script>
    <!-- end: JAVASCRIPTS REQUIRED FOR THIS PAGE ONLY -->
		<!-- start: CLIP-TWO JAVASCRIPTS -->
		<script src="assets/js/main.js"></script>
		<!-- start: JavaScript Event Handlers for this page -->
		<script src="assets/js/maps.js"></script>
    <script src="assets/js/ui-notifications.js"></script>
        <script src="assets/js/form-elements.js"></script>
		<script>
			jQuery(document).ready(function() {
				Main.init();
				Maps.init();
                UINotifications.init();
                FormElements.init();
			});
		</script>
		<!-- end: JavaScript Event Handlers for this page -->
		<!-- end: CLIP-TWO JAVASCRIPTS -->
	</body>
</html>
<?php else : ?>
    <div class="alert alert-warning"><?php _e('Only admins can view this content.'); ?></div>
    <?php header("Location: login.php"); exit(); ?>
<?php endif; ?>