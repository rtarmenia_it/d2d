<!DOCTYPE html>
<!-- Template Name: Clip-Two - Responsive Admin Template build with Twitter Bootstrap 3.x | Author: ClipTheme -->
<!--[if IE 8]><html class="ie8" lang="en"><![endif]-->
<!--[if IE 9]><html class="ie9" lang="en"><![endif]-->
<!--[if !IE]><!-->
<html lang="en">
	<!--<![endif]-->
	<!-- start: HEAD -->
    <?php include "templates/header.php" ?>
	<!-- end: HEAD -->
	<body>
		<div id="app">
			<!-- sidebar -->
            <?php include "templates/sidebar.php" ?>
			<!-- / sidebar -->
			<div class="app-content">
				<!-- start: TOP NAVBAR -->
                <?php include "templates/header-navbar.php" ?>
				<div class="main-content" >
					<div class="wrap-content container" id="container">
						<!-- start: PAGE TITLE -->
						<section id="page-title">
							<div class="row">
								<div class="col-sm-8">
									<h1 class="mainTitle">Շենք</h1>
									<span class="mainDescription">Ընտրել շենքը և լրացնել համապատասխան տվյալներ:</span>
								</div>
							</div>
						</section>
						<!-- end: PAGE TITLE -->

						<!-- start: SELECT BOXES -->
						<div class="container-fluid container-fullw bg-white">
							<div class="row">
								<div class="col-md-12">
									<div class="row">
										<form>
											<div class="col-sm-6">
												<div class="panel panel-transparent">
                                                    <div class="panel-body">
                                                        <div class="form-group">
                                                            <select class="js-example-placeholder-single js-states form-control state" name="state">
                                                                <option></option>
                                                                <option value="Երևան" data-id=Երևան>Երևան</option>
                                                                <option value="Լոռի" data-id=Լոռի>Լոռի</option>
                                                                <option value="Սյունիք" data-id=Սյունիք>Սյունիք</option>
                                                                <option value="Արագածոտն" data-id=Արագածոտն>Արագածոտն</option>
                                                                <option value="Կոտայք" data-id=Կոտայք>Կոտայք</option>
                                                                <option value="Արարատ" data-id=Արարատ>Արարատ</option>
                                                                <option value="Արմավիր" data-id=Արմավիր>Արմավիր</option>
                                                                <option value="Տավուշ" data-id=Տավուշ>Տավուշ</option>
                                                                <option value="Գեղարքունիք" data-id=Գեղարքունիք>Գեղարքունիք</option>
                                                                <option value="Շիրակ" data-id=Շիրակ>Շիրակ</option>
                                                                <option value="Վայոց Ձոր" data-id=Վայոց Ձոր>Վայոց Ձոր</option>
                                                            </select>
                                                        </div>
                                                        <div class="form-group">
                                                            <select class="js-example-placeholder-city js-states form-control" name="city" class="city"  id="city">
                                                                <option></option>
                                                            </select>
                                                        </div>
                                                        <div class="form-group">
                                                            <select class="js-example-placeholder-district js-states form-control" name="district" class="district" id="district">
                                                                <option></option>
                                                            </select>
                                                        </div>
                                                        <div class="form-group">
                                                            <select class="js-example-placeholder-street js-states form-control" name="street" class="street" id="street">
                                                                <option></option>
                                                            </select>
                                                        </div>
                                                        <div class="form-group">
                                                            <select class="js-example-placeholder-street_subtype js-states form-control" name="street_subtype" class="street_subtype" id="street_subtype">
                                                                <option></option>
                                                            </select>
                                                        </div>
                                                        <div class="form-group">
                                                            <select class="js-example-placeholder-house js-states form-control" name="building" class="building" id="house">
                                                                <option></option>
                                                            </select>
                                                        </div>
                                                        <div class="form-group" id="for_address_id">
                                                            <button class="btn btn-primary btn-block" id="find_building">Փնտրել</button>
                                                        </div>

                                                    </div>
												</div>
											</div>
										</form>
                                        <form id="for-shenq">
                                        <div class="col-sm-6">
                                            <div class="form-group">
                                            <fieldset>
                                                <legend>
                                                    Շենքի տվյալներ
                                                </legend>
                                                <div class="row">
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label>
                                                                Մուտքերի քանակ <span class="symbol required"></span>
                                                            </label>
                                                            <input type="text" placeholder="Մուտքերի քանակ" class="form-control" id="mutq" name="mutq"/>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label class="control-label">
                                                                Հարկերի թիվ <span class="symbol required"></span>
                                                            </label>
                                                            <input type="text" placeholder="Հարկերի թիվ" class="form-control" id="hark" name="hark"/>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label class="control-label">
                                                                Բնակարանների թիվ <span class="symbol required"></span>
                                                            </label>
                                                            <input type="text" placeholder="Բնակարանների թիվ" class="form-control" id="bnakaranneri_tiv" name="bnakaranneri_tiv"/>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label class="block">
                                                                Տուփի առկայություն
                                                            </label>
                                                            <div class="clip-radio radio-primary">
                                                                <input type="radio" id="tup-ka" name="tup" value="Կա">
                                                                <label for="tup-ka">
                                                                    Կա
                                                                </label>
                                                                <input type="radio" id="tup-chka" name="tup" value="Չկա">
                                                                <label for="tup-chka">
                                                                    Չկա
                                                                </label>
                                                        </div>
                                                    </div>
                                                </div>
                                                    </div>
                                                <div class="row">
                                                    <div class="col-md-sm-6">
                                                        <div class="form-group">

                                                            <div class="panel panel-white">
                                                                <div class="panel-heading">
                                                                    <div class="panel-title">
                                                                        Այլ մեկնաբանություններ
                                                                    </div>
                                                                </div>
                                                                <div class="panel-body">
                                                                    <div class="form-group">
                                                                        <div class="note-editor">
                                                                            <textarea class="form-control" id="comments" name="comments"></textarea>
                                                                        </div>
                                                                    </div>
                                                                </div>

                                                        </div>

                                                    </div>
                                                    </div>
                                                </div>
                                                    <button class="btn btn-danger btn-block" id="submit_shenq">Հաստատել</button>
                                            </fieldset>
                                            </div>
									</div>
                                            </form>

                                        </div>
								</div>
							</div>
						</div>
						<!-- end: SELECT BOXES -->
					</div>
				</div>
			</div>
			<!-- start: FOOTER -->
            <?php include "templates/footer.php" ?>
			<!-- end: FOOTER -->
		</div>
		<!-- start: MAIN JAVASCRIPTS -->
		<script src="vendor/jquery/jquery.min.js"></script>
		<script src="vendor/bootstrap/js/bootstrap.min.js"></script>
		<script src="vendor/modernizr/modernizr.js"></script>
		<script src="vendor/jquery-cookie/jquery.cookie.js"></script>
		<script src="vendor/perfect-scrollbar/perfect-scrollbar.min.js"></script>
		<script src="vendor/switchery/switchery.min.js"></script>
		<!-- end: MAIN JAVASCRIPTS -->
		<!-- start: JAVASCRIPTS REQUIRED FOR THIS PAGE ONLY -->
		<script src="vendor/maskedinput/jquery.maskedinput.min.js"></script>
		<script src="vendor/bootstrap-touchspin/jquery.bootstrap-touchspin.min.js"></script>
		<script src="vendor/autosize/autosize.min.js"></script>
		<script src="vendor/selectFx/classie.js"></script>
		<script src="vendor/selectFx/selectFx.js"></script>
		<script src="vendor/select2/select2.min.js"></script>
		<script src="vendor/bootstrap-datepicker/bootstrap-datepicker.min.js"></script>
		<script src="vendor/bootstrap-timepicker/bootstrap-timepicker.min.js"></script>
        <!-- start: JAVASCRIPTS REQUIRED FOR THIS PAGE ONLY -->
        <script src="vendor/sweetalert/sweet-alert.min.js"></script>
        <script src="vendor/toastr/toastr.min.js"></script>
        <!-- end: JAVASCRIPTS REQUIRED FOR THIS PAGE ONLY -->
		<!-- start: CLIP-TWO JAVASCRIPTS -->
		<script src="assets/js/main.js"></script>
		<!-- start: JavaScript Event Handlers for this page -->
        <script src="assets/js/form-elements.js"></script>
        <script src="assets/js/ui-notifications.js"></script>
		<script>
			jQuery(document).ready(function() {
				Main.init();
                UINotifications.init();
				FormElements.init();
			});
		</script>
        <script>
            $( "ul li:nth-child(1)" ).attr("class", "dropdown current-user");
            $( "ul li:nth-child(3)" ).attr("class", "");
            $( "ul li:nth-child(4)" ).attr("class", "");
            //$( "ul li:nth-child(5)" ).attr("class", "");
            //$( "ul li:nth-child(2)" ).attr("class", "");
        </script>
		<!-- end: JavaScript Event Handlers for this page -->
		<!-- end: CLIP-TWO JAVASCRIPTS -->
	</body>
</html>
