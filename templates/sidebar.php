<!-- sidebar -->
<div class="sidebar app-aside" id="sidebar">
<div class="sidebar-container perfect-scrollbar">
<nav>
<!-- start: MAIN NAVIGATION MENU -->
<div class="navbar-title">
    <span>Գլխավոր մենյու</span>
</div>
<ul class="main-navigation-menu">

<li class="">
        <a href="house.php">
            <div class="item-content">
                <div class="item-media">
                    <i class="ti-home"></i>
                </div>
                <div class="item-inner">
                    <span class="title"> Շենքի տվյալներ </span>
                </div>
            </div>
        </a>
</li>

<li>
    <a href="apt.php">
            <div class="item-content">
                <div class="item-media">
                    <i class="ti-home"></i>
                </div>
                <div class="item-inner">
                    <span class="title"> Բնակարանի տվյալներ </span>
                </div>
            </div>
        </a>
</li>

    <li>
        <a href="all_shenq.php">
            <div class="item-content">
                <div class="item-media">
                    <i class="ti-home"></i>
                </div>
                <div class="item-inner">
                    <span class="title"> Շենքեր </span>
                </div>
            </div>
        </a>
    </li>

    <li>
        <a href="all_apt.php">
            <div class="item-content">
                <div class="item-media">
                    <i class="ti-home"></i>
                </div>
                <div class="item-inner">
                    <span class="title"> Բնակարաններ </span>
                </div>
            </div>
        </a>
    </li>

</ul>
<!-- end: MAIN NAVIGATION MENU -->
</nav>
</div>
</div>
<!-- / sidebar -->