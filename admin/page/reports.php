<?php include_once('admin.php'); ?>
<div id="reports-message">
	<script src="assets/js/jquery.flot.min.js"></script>
	<script src="assets/js/jquery.flot.resize.min.js"></script>
	<?php include_once('admin.php'); ?>
	<?php include_once('../classes/reports.class.php'); ?>
	<fieldset>

		<legend><?php _e('Registered Users'); ?></legend><br>

	<!-- Datepicker form --><div id="message">

	<form method="post" action="page/reports.php" id="reports-date-form" class="form-inline">
		<?php _e('From:'); ?> <input name="start_date" value="<?php echo date('Y-m-d', $jigowatt_reports->start_date); ?>" id="from" type="date" class="hasDatePicker input-small">
		<?php _e('To:'); ?> <input name="end_date" value="<?php echo date('Y-m-d', $jigowatt_reports->end_date); ?>" id="to" type="date" class="hasDatePicker input-small">
		<button type="submit" id="reports-date-submit" class="btn" type="submit" data-loading-text="<?php _e('submitting...'); ?>"><?php _e('Submit'); ?></button>
	</form>