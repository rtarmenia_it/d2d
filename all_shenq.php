<?php include_once('classes/check.class.php'); ?>
<?php include_once('inc/db-func.php'); ?>
<?php if( protectThis("1, 3") ) : ?>
<!DOCTYPE html>
<!--[if IE 8]><html class="ie8" lang="en"><![endif]-->
<!--[if IE 9]><html class="ie9" lang="en"><![endif]-->
<!--[if !IE]><!-->
<html lang="en">
	<!--<![endif]-->
	<!-- start: HEAD -->
    <?php include "templates/header.php" ?>
	<body>
    <div class="modal"></div>
		<div id="app">
        <?php include "templates/sidebar.php" ?>

			<div class="app-content">
				<!-- start: TOP NAVBAR -->
                <?php include "templates/header-navbar.php" ?>
				<div class="main-content" >
					<div class="wrap-content container" id="container">
						<!-- start: BASIC MAP -->
						<div class="container-fluid container-fullw bg-white">
							<div class="row">
                            <div class="col-md-12">
                            <h5 class="over-title margin-bottom-15">Բոլոր <span class="text-bold">խմբագրված շենքերը</span></h5>
                                <div class="table-responsive">
                            <table class="table table-striped table-bordered table-hover table-full-width" id="sample_1">
                            <thead>
                            <tr>
                                <th>Edit</th>
                                <th>Մարզ</th>
                                <th>City</th>
                                <th>Region</th>
                                <th>Street</th>
                                <th>Sub__Street</th>
                                <th>Building</th>
                                <th>Մուտքերի քանակ</th>
                                <th>Հարկերի թիվ</th>
                                <th>Բնակարանների թիվ</th>
                                <th>Տուփի առկայություն</th>
                                <th>Այլ մեկնաբանություններ</th>
                            </tr>
                            </thead>
                                <tfoot>
                                <tr>
                                    <th>Edit</th>
                                    <th>Մարզ</th>
                                    <th>City</th>
                                    <th>Region</th>
                                    <th>Street</th>
                                    <th>Sub__Street</th>
                                    <th>Building</th>
                                    <th>Մուտքերի քանակ</th>
                                    <th>Հարկերի թիվ</th>
                                    <th>Բնակարանների թիվ</th>
                                    <th>Տուփի առկայություն</th>
                                    <th>Այլ մեկնաբանություններ</th>
                                </tr>
                                </tfoot>
                            <tbody>

                            </tbody>
                            </table>
                                </div>
                            </div>
							</div>
						</div>
						<!-- end: BASIC MAP -->
                        <!-- start: SELECT BOXES -->
                        <div class="container-fluid container-fullw bg-white">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="row">
                                        <form id="for-shenq-editmode">

                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- end: SELECT BOXES -->
					</div>
				</div>
			</div>
			<!-- start: FOOTER -->
            <?php include "templates/footer.php" ?>
			<!-- end: FOOTER -->
		</div>
		<!-- start: MAIN JAVASCRIPTS -->
		<script src="vendor/jquery/jquery.min.js"></script>
		<script src="vendor/bootstrap/js/bootstrap.min.js"></script>
		<script src="vendor/modernizr/modernizr.js"></script>
		<script src="vendor/jquery-cookie/jquery.cookie.js"></script>
		<script src="vendor/perfect-scrollbar/perfect-scrollbar.min.js"></script>
		<script src="vendor/switchery/switchery.min.js"></script>
		<!-- end: MAIN JAVASCRIPTS -->
		<!-- start: JAVASCRIPTS REQUIRED FOR THIS PAGE ONLY -->
		<script type="text/javascript" src="http://maps.google.com/maps/api/js?sensor=true"></script>
		<script src="vendor/gmaps/gmaps.js"></script>
        <!-- start: JAVASCRIPTS REQUIRED FOR THIS PAGE ONLY -->
        <script src="vendor/maskedinput/jquery.maskedinput.min.js"></script>
        <script src="vendor/bootstrap-touchspin/jquery.bootstrap-touchspin.min.js"></script>
        <script src="vendor/autosize/autosize.min.js"></script>
        <script src="vendor/selectFx/classie.js"></script>
        <script src="vendor/selectFx/selectFx.js"></script>
        <script src="vendor/select2/select2.min.js"></script>
        <script src="vendor/bootstrap-datepicker/bootstrap-datepicker.min.js"></script>
        <script src="vendor/bootstrap-timepicker/bootstrap-timepicker.min.js"></script>
    <!-- start: JAVASCRIPTS REQUIRED FOR THIS PAGE ONLY -->
    <script src="vendor/sweetalert/sweet-alert.min.js"></script>
    <script src="vendor/toastr/toastr.min.js"></script>
    <!-- end: JAVASCRIPTS REQUIRED FOR THIS PAGE ONLY -->

    <script src="https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>
    <script src="https://cdn.datatables.net/buttons/1.3.1/js/dataTables.buttons.min.js"></script>
    <script src="//cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
    <script src="//cdn.rawgit.com/bpampuch/pdfmake/0.1.27/build/pdfmake.min.js"></script>
    <script src="//cdn.rawgit.com/bpampuch/pdfmake/0.1.27/build/vfs_fonts.js"></script>
    <script src="//cdn.datatables.net/buttons/1.3.1/js/buttons.html5.min.js"></script>
    <!-- end: JAVASCRIPTS REQUIRED FOR THIS PAGE ONLY -->
		<!-- start: CLIP-TWO JAVASCRIPTS -->
		<script src="assets/js/main.js"></script>
		<!-- start: JavaScript Event Handlers for this page -->

        <script src="assets/js/form-elements.js"></script>
    <script src="assets/js/ui-notifications.js"></script>
        <script src="assets/js/table-data.js"></script>
		<script>
			jQuery(document).ready(function() {
				Main.init();
                UINotifications.init();
                //TableData.init();
                FormElements.init();

			});
		</script>
    <script>
        $( "ul li:nth-child(1)" ).attr("class", "dropdown current-user");
        $( "ul li:nth-child(2)" ).attr("class", "");
        $( "ul li:nth-child(3)" ).attr("class", "active open");
        $( "ul li:nth-child(5)" ).attr("class", "");
        $( "ul li:nth-child(4)" ).attr("class", "");
    </script>


    <script>
        $(document).ready(function() {
            $('#sample_1').DataTable( {

                "dom": 'Bfrtip',
                "buttons": [
                    'copyHtml5',
                    'excelHtml5',
                    'csvHtml5'
                ],

                "processing": true,
                "serverSide": true,
                "ajax": "inc/fetchAllShenq.php",
                "aoColumnDefs" : [{
                    "aTargets" : [0]
                }],
                "oLanguage" : {
                    "sLengthMenu" : "Ցուցադրել _MENU_ տող",
                    "sSearch" : "",
                    "oPaginate" : {
                        "sPrevious" : "Նախորդ",
                        "sNext" : "Հաջորդ"
                    }
                },
                "aaSorting" : [[1, 'asc']],
                "aLengthMenu" : [[5, 10, 15, 20, -1], [5, 10, 15, 20, "All"] // change per page values here
                ],
                // set the initial value
                "iDisplayLength" : 10,


                initComplete: function () {
                    this.api().columns().every( function () {
                        var column = this;
                        var select = $('<select class="cs-select cs-skin-elastic"><option value=""></option></select>')
                            .appendTo( $(column.footer()).empty() )
                            .on( 'change', function () {
                                var val = $.fn.dataTable.util.escapeRegex(
                                    $(this).val()
                                );

                                column
                                    .search( val ? ''+val+'' : '', true, false )
                                    .draw();
                            } );

                        column.data().unique().sort().each( function ( d, j ) {
                            select.append( '<option value="'+d+'">'+d+'</option>' )
                        } );
                    } );
                }
            } );
        } );
    </script>
		<!-- end: JavaScript Event Handlers for this page -->
		<!-- end: CLIP-TWO JAVASCRIPTS -->
	</body>
</html>
<?php else : ?>
    <div class="alert alert-warning"><?php _e('Only admins can view this content.'); ?></div>
    <?php header("Location: login.php"); exit(); ?>
<?php endif; ?>