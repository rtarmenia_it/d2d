<?php
require_once 'oracle-db-settings.php';

function fetchAllIPTvTariffs($serviceName){
    
    $tpdesc = array('"Հիբրիդ ԹիՎի" - Պրեմիում'=>'120 ալիք', '"Հիբրիդ ԹիՎի" - Ստարտ'=>'45 ալիք', '"Հիբրիդ ԹիՎի"- Օպտիմում'=>'80 ալիք');

        $db = oracleConnect();
        $results = "<p>ԱյՓի Հեռուստատեսությունը լավագույն լուծումն է Ձեր հեռուստաժամանցն ապահովելու համար։<br>
Բաժանորդագրվե՛ք և ստացե՛ք եթերը ամբողջովին կառավարելու հնարավորություն:</p>";
        // First, check that the specified field exists.  Very important as we are using other unsanitized data in the following query.
        $query = "SELECT
OS_BILL.CONTRACT_CLASSES.NAME,
OS_BILL.TARIFF_PLANS.PRINT_NAME,
OS_BILL.PERIODIC_SERVE_HISTORY.PRICE,
OS_BILL.TARIFF_PLANS.TARIFF_PLAN_ID,
OS_BILL.CONTRACT_CLASSES.CONTRACT_CLASS_ID
FROM
OS_BILL.CONTRACT_CLASSES
INNER JOIN OS_BILL.TARIFF_PLANS ON OS_BILL.TARIFF_PLANS.CONTRACT_CLASS_ID = OS_BILL.CONTRACT_CLASSES.CONTRACT_CLASS_ID
INNER JOIN OS_BILL.PERIODIC_SERVES ON OS_BILL.PERIODIC_SERVES.TARIFF_PLAN_ID = OS_BILL.TARIFF_PLANS.TARIFF_PLAN_ID
INNER JOIN OS_BILL.PERIODIC_SERVE_HISTORY ON OS_BILL.PERIODIC_SERVE_HISTORY.PERIODIC_SERVE_ID = OS_BILL.PERIODIC_SERVES.PERIODIC_SERVE_ID
WHERE
OS_BILL.TARIFF_PLANS.CONNECTION_END_DATE IS NULL AND
OS_BILL.TARIFF_PLANS.CLIENT_TYPE_ACCESS = 1 AND
OS_BILL.PERIODIC_SERVE_HISTORY.CONTRACT_ID IS NULL AND
OS_BILL.PERIODIC_SERVE_HISTORY.END_DATE IS NULL AND
OS_BILL.PERIODIC_SERVE_HISTORY.PRICE <> 0 AND
OS_BILL.PERIODIC_SERVE_HISTORY.PRICE <> '1500' AND
OS_BILL.CONTRACT_CLASSES.NAME = '$serviceName'
ORDER BY
OS_BILL.PERIODIC_SERVE_HISTORY.PRICE ASC
";
        $stmt = oci_parse($db, $query);
                    oci_execute($stmt);
                    while (oci_fetch($stmt)) {
                        $tpname = oci_result($stmt, 'PRINT_NAME');
                        $price = oci_result($stmt, 'PRICE');
                        $serviceID = oci_result($stmt, 'CONTRACT_CLASS_ID');
                        $tariffID = oci_result($stmt, 'TARIFF_PLAN_ID');
                        
                        if (array_key_exists($tpname, $tpdesc)) {
                            $desc = $tpdesc[$tpname];
                            
                        }
                        
                        $results .= "<label>
                            <section>								
                                <input type='radio' name='tvradio' id='radiotv' value='2 $tariffID'>
                                <p>$tpname</p>
                                <p>$desc</p>
                                <section class='price'><p>$price դր․/ամիս</p></section>
                            </section>
                        </label>";
}
            
            $results .= "<span class='terms_link'>Մանրամասն պայմաններին կարող եք ծանոթանալ <a href='http://www.rtarmenia.am/am/tv/' style = 'color:rgb(151, 33, 133)' target='_blank'>այս հղումով</a></span>"
                    . "<button id='tv_cancel' type='button'>Չեղարկել</button>";

        $stmt = null;
        return $results;
}


function fetchAllInetTariffs($serviceName){
    
    $tpdesc = array('Ռինտերնետ 7500'=>'15 Մբ/վ','Ռինտերնետ 10000'=>'25 Մբ/վ','Ռինտերնետ տարեկան 10000'=>'50 Մբ/վ 1-ին ամիս և Wifi 2 դրամով +  կրկնապատիկ արագություն 1 տարի, պլանշետ-սմարթֆոն հատուկ գնով',
                    'Ռինտերնետ գործընկեր 7500'=>'15 Մբ/վ + գիշերային կրկնապատիկ արագություն + Wifi 1 դրամով','Ռինտերնետ տարեկան 6000'=>'6 Մբ/վ 1-ին ամիս 1 դրամով, Wifi սարք հատուկ գնով',
                    'Ռինտերնետ 6000'=>'6 Մբ/վ','Ռինտերնետ գործընկեր 6000'=>'6 Մբ/վ + գիշերային կրկնապատիկ արագություն','Ռինտերնետ գործընկեր 7000'=>'10Մբ/վ + գիշերային կրկնապատիկ արագություն',
                    'Ռինտերնետ տարեկան 7500'=>'50 Մբ/վ 1-ին ամիս և Wifi 2 դրամով + կրկնապատիկ արագություն 1 տարի, պլանշետ-սմարթֆոն հատուկ գնով','Ռինտերնետ 7000'=>'10 Մբ/վ',
                    'Ռինտերնետ գործընկեր 10000'=>'25 Մբ/վ + գիշերային կրկնապատիկ արագություն + Wifi 1 դրամով','Ռինտերնետ տարեկան 7000'=>'10 Մբ/վ 1-ին ամիս 1 դրամով + գիշերային կրկնապատիկ արագություն, Wifi սարք հատուկ գնով');

        $db = oracleConnect();
        $results = "";
        // First, check that the specified field exists.  Very important as we are using other unsanitized data in the following query.
        $query = "SELECT
OS_BILL.CONTRACT_CLASSES.NAME,
OS_BILL.TARIFF_PLANS.PRINT_NAME,
OS_BILL.PERIODIC_SERVE_HISTORY.PRICE,
OS_BILL.PERIODIC_SERVES.PERIODIC_SERVE_ID,
OS_BILL.TARIFF_PLANS.TARIFF_PLAN_ID
FROM
OS_BILL.CONTRACT_CLASSES
INNER JOIN OS_BILL.TARIFF_PLANS ON OS_BILL.TARIFF_PLANS.CONTRACT_CLASS_ID = OS_BILL.CONTRACT_CLASSES.CONTRACT_CLASS_ID
INNER JOIN OS_BILL.PERIODIC_SERVES ON OS_BILL.PERIODIC_SERVES.TARIFF_PLAN_ID = OS_BILL.TARIFF_PLANS.TARIFF_PLAN_ID
INNER JOIN OS_BILL.PERIODIC_SERVE_HISTORY ON OS_BILL.PERIODIC_SERVE_HISTORY.PERIODIC_SERVE_ID = OS_BILL.PERIODIC_SERVES.PERIODIC_SERVE_ID
WHERE
OS_BILL.TARIFF_PLANS.CONNECTION_END_DATE IS NULL AND
OS_BILL.TARIFF_PLANS.CLIENT_TYPE_ACCESS = 1 AND
OS_BILL.PERIODIC_SERVE_HISTORY.CONTRACT_ID IS NULL AND
OS_BILL.PERIODIC_SERVE_HISTORY.END_DATE IS NULL AND
OS_BILL.PERIODIC_SERVE_HISTORY.PRICE <> 0 AND
OS_BILL.PERIODIC_SERVE_HISTORY.PRICE <> '1500' AND
OS_BILL.TARIFF_PLANS.PRINT_NAME IN ('Ռինտերնետ 7500', 'Ռինտերնետ 10000', 'Ռինտերնետ 6000', 'Ռինտերնետ 7000')
ORDER BY
OS_BILL.PERIODIC_SERVE_HISTORY.PRICE ASC
";
        $stmt = oci_parse($db, $query);
                    oci_execute($stmt);
                    while (oci_fetch($stmt)) {
                        $tpname = oci_result($stmt, 'PRINT_NAME');
                        $price = oci_result($stmt, 'PRICE');
                        $tariffID = oci_result($stmt, 'TARIFF_PLAN_ID');
                        if (array_key_exists($tpname, $tpdesc)) {
                            $desc = $tpdesc[$tpname];
                        }
                        
                        $results .= "<label>
                                <section>
                                    <input type='radio' name='internetradio' id='radiointernet' value='1 $tariffID'>
                                    <p>$tpname</p>
                                    <p>$desc</p>
                                    <section class='price'><p>$price դր․/ամիս</p></section>
                                </section>
                            </label>";
}
            
            $results .= "<span class='terms_link'>Մանրամասն պայմաններին կարող եք ծանոթանալ <a href='http://www.rtarmenia.am/am/home/home-internet/rinternet/' style = 'color:rgb(143, 197, 71)' target='_blank'>այս հղումով</a></span>"
                    . "<button id='internet_cancel' type='button'>Չեղարկել</button>";

        $stmt = null;
        return $results;
}


function fetchAllTrios($serviceName){

    $tpdesc = array('"Տրիո կոնստրուկտոր" - Երկնագույն'=>'33 Մբ/վ + 45 ալիք + 200 րոպե', '"Տրիո կոնստրուկտոր ժամկետային" - Սև'=>'33 Մբ/վ + 80 ալիք + 200 րոպե, 1-ին ամիսը և Wifi ծառայությունն անվճար',
        '"Տրիո կոնստրուկտոր" - Նարնջագույն'=>'33 Մբ/վ + 120 ալիք + 200 րոպե', '"Տրիո կոնստրուկտոր" - Սև'=>'33 Մբ/վ + 80 ալիք + 200 րոպե',
        '"Տրիո կոնստրուկտոր" - Կարմիր'=>'22 Մբ/վ + 120 ալիք + 200 րոպե', '"Տրիո կոնստրուկտոր" - Սպիտակ'=>'11 Մբ/վ + 80 ալիք + 200 րոպե',
        '"Տրիո կոնստրուկտոր" - Կանաչ'=>'11 Մբ/վ + 45 ալիք + 200 րոպե', '"Տրիո կոնստրուկտոր" - Դեղին'=>'22 Մբ/վ + 45 ալիք + 200 րոպե',
        '"Տրիո կոնստրուկտոր ժամկետային" - Սպիտակ'=>'11 Մբ/վ + 80 ալիք + 200 րոպե, 1-ին ամիսը և Wifi ծառայությունն անվճար',
        '"Տրիո կոնստրուկտոր ժամկետային" - Մանուշակագույն'=>'22 Մբ/վ + 80 ալիք + 200 րոպե, 1-ին ամիսը և Wifi ծառայությունն անվճար',
        '"Տրիո կոնստրուկտոր ժամկետային" - Նարնջագույն'=>'33 Մբ/վ + 120 ալիք + 200 րոպե, 1-ին ամիսը և Wifi ծառայությունն անվճար',
        '"Տրիո կոնստրուկտոր" - Մանուշակագույն'=>'22 Մբ/վ + 80 ալիք + 200 րոպե', '"Տրիո կոնստրուկտոր ժամկետային" - Կարմիր'=>'22 Մբ/վ + 120 ալիք + 200 րոպե, 1-ին ամիսը և Wifi ծառայությունն անվճար',
        '"Տրիո կոնստրուկտոր" - Կապույտ'=>'11 Մբ/վ + 120 ալիք + 200 րոպե', '"Տրիո կոնստրուկտոր ժամկետային" - Կանաչ'=>'11 Մբ/վ + 45 ալիք + 200 րոպե, 1-ին ամիսը և Wifi ծառայությունն անվճար',
        '"Տրիո կոնստրուկտոր ժամկետային" - Կապույտ'=>'11 Մբ/վ + 120 ալիք + 200 րոպե, 1-ին ամիսը և Wifi ծառայությունն անվճար',
        '"Տրիո կոնստրուկտոր ժամկետային" - Դեղին'=>'22 Մբ/վ + 45 ալիք + 200 րոպե, 1-ին ամիսը և Wifi ծառայությունն անվճար', '"Տրիո կոնստրուկտոր ժամկետային" - Երկնագույն'=>'33 Մբ/վ + 45 ալիք + 200 րոպե, 1-ին ամիսը և Wifi ծառայությունն անվճար');

    $db = oracleConnect();
    $results = "";
    // First, check that the specified field exists.  Very important as we are using other unsanitized data in the following query.
    $query = "SELECT
cc.NAME,
OS_BILL.TARIFF_PLANS.PRINT_NAME,
Sum(OS_BILL.PERIODIC_SERVE_HISTORY.PRICE) AS TOTAL_PRICE,
OS_BILL.TARIFF_PLANS.TARIFF_PLAN_ID
FROM
OS_BILL.CONTRACT_CLASSES cc
INNER JOIN OS_BILL.TARIFF_PLANS ON OS_BILL.TARIFF_PLANS.CONTRACT_CLASS_ID = cc.CONTRACT_CLASS_ID
INNER JOIN OS_BILL.PERIODIC_SERVES ON OS_BILL.PERIODIC_SERVES.TARIFF_PLAN_ID = OS_BILL.TARIFF_PLANS.TARIFF_PLAN_ID
INNER JOIN OS_BILL.PERIODIC_SERVE_HISTORY ON OS_BILL.PERIODIC_SERVE_HISTORY.PERIODIC_SERVE_ID = OS_BILL.PERIODIC_SERVES.PERIODIC_SERVE_ID
WHERE
OS_BILL.TARIFF_PLANS.CONNECTION_END_DATE IS NULL AND
OS_BILL.TARIFF_PLANS.CLIENT_TYPE_ACCESS = 1 AND
OS_BILL.PERIODIC_SERVE_HISTORY.CONTRACT_ID IS NULL AND
OS_BILL.PERIODIC_SERVE_HISTORY.END_DATE IS NULL AND
OS_BILL.PERIODIC_SERVE_HISTORY.PRICE <> 0 AND
OS_BILL.PERIODIC_SERVE_HISTORY.PRICE <> '1500' AND
CC.NAME = '$serviceName' AND
OS_BILL.TARIFF_PLANS.PRINT_NAME IN ('"."\""."Տրիո կոնստրուկտոր"."\""." - Դեղին', '"."\""."Տրիո կոնստրուկտոր"."\""." - Երկնագույն', '"."\""."Տրիո կոնստրուկտոր"."\""." - Նարնջագույն', '"."\""."Տրիո կոնստրուկտոր"."\""." - Սև', '"."\""."Տրիո կոնստրուկտոր"."\""." - Կարմիր', '"."\""."Տրիո կոնստրուկտոր"."\""." - Սպիտակ', '"."\""."Տրիո կոնստրուկտոր"."\""." - Կանաչ', '"."\""."Տրիո կոնստրուկտոր"."\""." - Մանուշակագույն', '"."\""."Տրիո կոնստրուկտոր"."\""." - Կապույտ') AND
OS_BILL.TARIFF_PLANS.PRINT_NAME NOT LIKE '%աշխատակից%' AND
OS_BILL.TARIFF_PLANS.PRINT_NAME NOT LIKE '%ժամկետային +%'
GROUP BY
cc.NAME, OS_BILL.TARIFF_PLANS.PRINT_NAME, OS_BILL.TARIFF_PLANS.TARIFF_PLAN_ID
ORDER BY
TOTAL_PRICE ASC
";
    $stmt = oci_parse($db, $query);
    oci_execute($stmt);
    while (oci_fetch($stmt)) {
        $tpname = oci_result($stmt, 'PRINT_NAME');
        $price = oci_result($stmt, 'TOTAL_PRICE');
        $tariffID = oci_result($stmt, 'TARIFF_PLAN_ID');
        if (array_key_exists($tpname, $tpdesc)) {
            $desc = $tpdesc[$tpname];
            $tpname  = str_replace('"', '', $tpname);
        }

        $results .= "<label>"
            . "<section>"
            . "<input type='radio' name='trioradio' id='radiotrio' value='4 $tariffID'>"
            . "<p>$tpname</p>"
            . "<p>$desc</p>"
            . "<section class='price'>"
            . "<p>$price դր․/ամիս</p>"
            . "</section></section></label>";
    }

    $results .= "<span class='terms_link'>Մանրամասն պայմաններին կարող եք ծանոթանալ <a href='http://rtarmenia.am/trio' target='_blank'>այս հղումով</a></span>"
        . "<button id='trio_cancel' type='button'>Չեղարկել</button></section>";

    $stmt = null;
    return $results;
}

function fetchAllJamketayinTrios($serviceName){
    
    $tpdesc = array('"Տրիո կոնստրուկտոր" - Երկնագույն'=>'33 Մբ/վ + 45 ալիք + 200 րոպե', '"Տրիո կոնստրուկտոր ժամկետային" - Սև'=>'33 Մբ/վ + 80 ալիք + 200 րոպե<br>1-ին ամիսը և Wifi ծառայությունն անվճար', 
                    '"Տրիո կոնստրուկտոր" - Նարնջագույն'=>'33 Մբ/վ + 120 ալիք + 200 րոպե', '"Տրիո կոնստրուկտոր" - Սև'=>'33 Մբ/վ + 80 ալիք + 200 րոպե', 
                    '"Տրիո կոնստրուկտոր" - Կարմիր'=>'22 Մբ/վ + 120 ալիք + 200 րոպե', '"Տրիո կոնստրուկտոր" - Սպիտակ'=>'11 Մբ/վ + 80 ալիք + 200 րոպե', 
                    '"Տրիո կոնստրուկտոր" - Կանաչ'=>'11 Մբ/վ + 45 ալիք + 200 րոպե', '"Տրիո կոնստրուկտոր" - Դեղին'=>'22 Մբ/վ + 45 ալիք + 200 րոպե', 
                    '"Տրիո կոնստրուկտոր ժամկետային" - Սպիտակ'=>'11 Մբ/վ + 80 ալիք + 200 րոպե<br>1-ին ամիսը և Wifi ծառայությունն անվճար', 
                    '"Տրիո կոնստրուկտոր ժամկետային" - Մանուշակագույն'=>'22 Մբ/վ + 80 ալիք + 200 րոպե<br>1-ին ամիսը և Wifi ծառայությունն անվճար', 
                    '"Տրիո կոնստրուկտոր ժամկետային" - Նարնջագույն'=>'33 Մբ/վ + 120 ալիք + 200 րոպե<br>1-ին ամիսը և Wifi ծառայությունն անվճար', 
                    '"Տրիո կոնստրուկտոր" - Մանուշակագույն'=>'22 Մբ/վ + 80 ալիք + 200 րոպե', '"Տրիո կոնստրուկտոր ժամկետային" - Կարմիր'=>'22 Մբ/վ + 120 ալիք + 200 րոպե<br>1-ին ամիսը և Wifi ծառայությունն անվճար', 
                    '"Տրիո կոնստրուկտոր" - Կապույտ'=>'11 Մբ/վ + 120 ալիք + 200 րոպե', '"Տրիո կոնստրուկտոր ժամկետային" - Կանաչ'=>'11 Մբ/վ + 45 ալիք + 200 րոպե<br>1-ին ամիսը և Wifi ծառայությունն անվճար', 
                    '"Տրիո կոնստրուկտոր ժամկետային" - Կապույտ'=>'11 Մբ/վ + 120 ալիք + 200 րոպե<br>1-ին ամիսը և Wifi ծառայությունն անվճար', 
                    '"Տրիո կոնստրուկտոր ժամկետային" - Դեղին'=>'22 Մբ/վ + 45 ալիք + 200 րոպե<br>1-ին ամիսը և Wifi ծառայությունն անվճար', '"Տրիո կոնստրուկտոր ժամկետային" - Երկնագույն'=>'33 Մբ/վ + 45 ալիք + 200 րոպե<br>1-ին ամիսը և Wifi ծառայությունն անվճար');

        $db = oracleConnect();
        $results = "";
        // First, check that the specified field exists.  Very important as we are using other unsanitized data in the following query.
        $query = "SELECT
cc.NAME,
OS_BILL.TARIFF_PLANS.PRINT_NAME,
Sum(OS_BILL.PERIODIC_SERVE_HISTORY.PRICE) AS TOTAL_PRICE,
OS_BILL.TARIFF_PLANS.TARIFF_PLAN_ID
FROM
OS_BILL.CONTRACT_CLASSES cc
INNER JOIN OS_BILL.TARIFF_PLANS ON OS_BILL.TARIFF_PLANS.CONTRACT_CLASS_ID = cc.CONTRACT_CLASS_ID
INNER JOIN OS_BILL.PERIODIC_SERVES ON OS_BILL.PERIODIC_SERVES.TARIFF_PLAN_ID = OS_BILL.TARIFF_PLANS.TARIFF_PLAN_ID
INNER JOIN OS_BILL.PERIODIC_SERVE_HISTORY ON OS_BILL.PERIODIC_SERVE_HISTORY.PERIODIC_SERVE_ID = OS_BILL.PERIODIC_SERVES.PERIODIC_SERVE_ID
WHERE
OS_BILL.TARIFF_PLANS.CONNECTION_END_DATE IS NULL AND
OS_BILL.TARIFF_PLANS.CLIENT_TYPE_ACCESS = 1 AND
OS_BILL.TARIFF_PLANS.DISCOUNT_PERIOD = 1 AND
OS_BILL.PERIODIC_SERVE_HISTORY.CONTRACT_ID IS NULL AND
OS_BILL.PERIODIC_SERVE_HISTORY.END_DATE IS NULL AND
OS_BILL.PERIODIC_SERVE_HISTORY.PRICE <> 0 AND
OS_BILL.PERIODIC_SERVE_HISTORY.PRICE <> '1500' AND
CC.NAME = '$serviceName'
GROUP BY
cc.NAME, OS_BILL.TARIFF_PLANS.PRINT_NAME, OS_BILL.TARIFF_PLANS.TARIFF_PLAN_ID
ORDER BY
TOTAL_PRICE ASC
";
        $stmt = oci_parse($db, $query);
                    oci_execute($stmt);
                    while (oci_fetch($stmt)) {
                        $tpname = oci_result($stmt, 'PRINT_NAME');
                        $price = oci_result($stmt, 'TOTAL_PRICE');
                        $tariffID = oci_result($stmt, 'TARIFF_PLAN_ID');
                        if (array_key_exists($tpname, $tpdesc)) {
                            $desc = $tpdesc[$tpname];
                            $tpname  = str_replace('"', '', $tpname);
                        }
                        
                        $results .= "<label>"
                                . "<section>"
                                . "<input type='radio' id='radio05' name='trioradio' id='radiotrio' value='4 $tariffID'>"
                                . "<p>$tpname</p>"
                                . "<p>$desc</p>"
                                . "<section class='price'>"
                                . "<p>$price դր․/ամիս</p>"
                                . "</section></section></label>";
}
            
            $results .= "<span class='terms_link'>Մանրամասն պայմաններին կարող եք ծանոթանալ <a href='http://rtarmenia.am/trio' target='_blank'>այս հղումով</a></span>
                        <button id='trio_cancel' type='button'>Չեղարկել</button></section>";

        $stmt = null;
        return $results;
}

function fetchAllDuos($serviceName){

        $db = oracleConnect();
        $tpdesc = array('Դուո Օպտի'=>'10 Մբ/վ + 5 ալիք + 2500 դրամ պարտադիր ամսական հավելավճարի չափով հեռուստաալիքներ, փաթեթի առաջին ամսավճարն ու Wi-Fi ծառայությունն անվճար', 'Դուո Մինի'=>'5 Մբ/վ + 5 ալիք + 1000 դրամ պարտադիր ամսական հավելավճարի չափով հեռուստաալիքներ, փաթեթի առաջին ամսավճարն անվճար', 
                        'Դուո'=>'5 Մբ/վ + 5 ալիք', 'Դուո Մաքսի'=>'15 Մբ/վ + 5 ալիք + 3500 դրամ պարտադիր ամսական հավելավճարի չափով հեռուստաալիքներ, փաթեթի առաջին ամսավճարն ու Wi-Fi ծառայությունն անվճար');
        
        $results = "<p>Ցանկանու՞մ եք ունենալ գերարագ ինտերնետ և դիտել միայն սիրելի հեռուստաալիքներ։<br>
Միացե՛ք Դուո ծառայությունների փաթեթներից որևէ մեկին և ստացե՛ք ինտերնետի ու հեռուստատեսության Ձեր նախընտրած համադրությունը:
                        </p>";
        // First, check that the specified field exists.  Very important as we are using other unsanitized data in the following query.
        $query = "SELECT
OS_BILL.CONTRACT_CLASSES.NAME,
OS_BILL.TARIFF_PLANS.PRINT_NAME,
OS_BILL.PERIODIC_SERVE_HISTORY.PRICE,
OS_BILL.PERIODIC_SERVES.PERIODIC_SERVE_ID,
OS_BILL.TARIFF_PLANS.TARIFF_PLAN_ID
FROM
OS_BILL.CONTRACT_CLASSES
INNER JOIN OS_BILL.TARIFF_PLANS ON OS_BILL.TARIFF_PLANS.CONTRACT_CLASS_ID = OS_BILL.CONTRACT_CLASSES.CONTRACT_CLASS_ID
INNER JOIN OS_BILL.PERIODIC_SERVES ON OS_BILL.PERIODIC_SERVES.TARIFF_PLAN_ID = OS_BILL.TARIFF_PLANS.TARIFF_PLAN_ID
INNER JOIN OS_BILL.PERIODIC_SERVE_HISTORY ON OS_BILL.PERIODIC_SERVE_HISTORY.PERIODIC_SERVE_ID = OS_BILL.PERIODIC_SERVES.PERIODIC_SERVE_ID
WHERE
OS_BILL.TARIFF_PLANS.CONNECTION_END_DATE IS NULL AND
OS_BILL.TARIFF_PLANS.CLIENT_TYPE_ACCESS = 1 AND
OS_BILL.PERIODIC_SERVE_HISTORY.CONTRACT_ID IS NULL AND
OS_BILL.PERIODIC_SERVE_HISTORY.END_DATE IS NULL AND
OS_BILL.PERIODIC_SERVE_HISTORY.PRICE <> 0 AND
OS_BILL.PERIODIC_SERVE_HISTORY.PRICE <> '1500' AND
OS_BILL.CONTRACT_CLASSES.NAME = '$serviceName' AND
(OS_BILL.TARIFF_PLANS.TARIFF_PLAN_ID <> 2688 AND
OS_BILL.TARIFF_PLANS.TARIFF_PLAN_ID <> 2689)
ORDER BY
OS_BILL.PERIODIC_SERVE_HISTORY.PRICE ASC,
OS_BILL.TARIFF_PLANS.TARIFF_PLAN_ID ASC
";
        $stmt = oci_parse($db, $query);
                    oci_execute($stmt);
                    while (oci_fetch($stmt)) {
                        $tpname = oci_result($stmt, 'PRINT_NAME');
                        $price = oci_result($stmt, 'PRICE');
                        $tariffID = oci_result($stmt, 'TARIFF_PLAN_ID');
                        if (array_key_exists($tpname, $tpdesc)) {
                            $desc = $tpdesc[$tpname];
                        }
                        
                        $results .= "<label>
                            <section>
                                <input type='radio' id='radio01' name='duoradio' id='radioduo' value='5 $tariffID'>
                                <p>$tpname</p>
                                <p>$desc</p>
                                <section class='price'><p>$price դր․/ամիս</p></section>
                            </section>
                        </label>";
}
            
            $results .= "<span class='terms_link'>Մանրամասն պայմաններին կարող եք ծանոթանալ <a href='http://rtarmenia.am/duo' target='_blank'>այս հղումով</a></span>"
                    . "<button id='duo_cancel' type='button'>Չեղարկել</button></section>";

        $stmt = null;
        return $results;
}


function fetchAllTel($serviceName){
    $tpdesc = array('Ֆիքսլայն գործընկեր - Էկոնոմ'=>'5 դր./ր ՀՀ ֆիքսված ցանցեր, 19դր./ր ՀՀ բջջային ցանցեր', 
                    'Ֆիքսլայն - Էկոնոմ'=>'5 դր./ր ՀՀ ֆիքսված ցանցեր <br> 19դր./ր ՀՀ բջջային ցանցեր');

        $db = oracleConnect();
        $results = "<p>Շփվե՛ք մտերիմներիդ հետ և կատարե՛ք միջազգային զանգեր մատչելի սակագներով Ռոստելեկոմի նոր սերնդի բարձրորակ հեռախոսակապի միջոցով։</p>";
        // First, check that the specified field exists.  Very important as we are using other unsanitized data in the following query.
        $query = "SELECT
OS_BILL.CONTRACT_CLASSES.NAME,
OS_BILL.TARIFF_PLANS.PRINT_NAME,
OS_BILL.PERIODIC_SERVE_HISTORY.PRICE,
OS_BILL.PERIODIC_SERVES.PERIODIC_SERVE_ID,
OS_BILL.TARIFF_PLANS.TARIFF_PLAN_ID
FROM
OS_BILL.CONTRACT_CLASSES
INNER JOIN OS_BILL.TARIFF_PLANS ON OS_BILL.TARIFF_PLANS.CONTRACT_CLASS_ID = OS_BILL.CONTRACT_CLASSES.CONTRACT_CLASS_ID
INNER JOIN OS_BILL.PERIODIC_SERVES ON OS_BILL.PERIODIC_SERVES.TARIFF_PLAN_ID = OS_BILL.TARIFF_PLANS.TARIFF_PLAN_ID
INNER JOIN OS_BILL.PERIODIC_SERVE_HISTORY ON OS_BILL.PERIODIC_SERVE_HISTORY.PERIODIC_SERVE_ID = OS_BILL.PERIODIC_SERVES.PERIODIC_SERVE_ID
WHERE
OS_BILL.TARIFF_PLANS.CONNECTION_END_DATE IS NULL AND
OS_BILL.TARIFF_PLANS.CLIENT_TYPE_ACCESS = 1 AND
OS_BILL.PERIODIC_SERVE_HISTORY.CONTRACT_ID IS NULL AND
OS_BILL.PERIODIC_SERVE_HISTORY.END_DATE IS NULL AND
OS_BILL.PERIODIC_SERVE_HISTORY.PRICE <> 0 AND
OS_BILL.PERIODIC_SERVE_HISTORY.PRICE <> '1500' AND
OS_BILL.CONTRACT_CLASSES.NAME = '$serviceName' AND
OS_BILL.TARIFF_PLANS.TARIFF_PLAN_ID = 2052
ORDER BY
OS_BILL.PERIODIC_SERVE_HISTORY.PRICE ASC
";
        $stmt = oci_parse($db, $query);
                    oci_execute($stmt);
                    while (oci_fetch($stmt)) {
                        $tpname = oci_result($stmt, 'PRINT_NAME');
                        $price = oci_result($stmt, 'PRICE');
                        $tpid = oci_result($stmt, 'TARIFF_PLAN_ID');
                        if (array_key_exists($tpname, $tpdesc)) {
                            $desc = $tpdesc[$tpname];
                        }
                        
                        $results .= "<label>
                            <section>
                                <input type='radio' name='telradio' id='radiotel' value='3 $tpid'>	
                                <p>$tpname</p>
                                <p>$desc</p>
                                <section class='price'><p>$price դր․/ամիս</p></section>
                            </section>
                        </label>";
}
            
            $results .= "<span class='terms_link'>Մանրամասն պայմաններին կարող եք ծանոթանալ <a href='http://www.rtarmenia.am/am/home/home-telephony/fixline/' style = 'color:rgb(240, 118, 44)' target='_blank'>այս հղումով</a></span>"
                    . "<button id='tel_cancel' type='button'>Չեղարկել</button>";

        $stmt = null;
        return $results;
}

function fetchAllJamketayin($serviceName){
    
    $tpdesc = array('Ռինտերնետ 7500'=>'15 Մբ/վ','Ռինտերնետ 10000'=>'25 Մբ/վ','Ռինտերնետ տարեկան 10000'=>'25 Մբ/վ 1-ին ամիս 1 դրամով, Wifi ծառայություն 1 դրամով, 2 հատ պլանշետ-սմարթֆոն հատուկ գնով, կրկնապատիկ արագություն',
                    'Ռինտերնետ գործընկեր 7500'=>'15 Մբ/վ + գիշերային կրկնապատիկ արագություն + Wifi 1 դրամով','Ռինտերնետ տարեկան 6000'=>'6 Մբ/վ 1-ին ամիս 1 դրամով, Wifi սարք հատուկ գնով',
                    'Ռինտերնետ 6000'=>'6 Մբ/վ','Ռինտերնետ գործընկեր 6000'=>'6 Մբ/վ + գիշերային կրկնապատիկ արագություն','Ռինտերնետ գործընկեր 7000'=>'10Մբ/վ + գիշերային կրկնապատիկ արագություն',
                    'Ռինտերնետ տարեկան 7500'=>'15 Մբ/վ 1-ին ամիս 1 դրամով, Wifi ծառայություն 1 դրամով, 1 հատ պլանշետ-սմարթֆոն հատուկ գնով, կրկնապատիկ արագություն','Ռինտերնետ 7000'=>'10 Մբ/վ',
                    'Ռինտերնետ գործընկեր 10000'=>'25 Մբ/վ + գիշերային կրկնապատիկ արագություն + Wifi 1 դրամով','Ռինտերնետ տարեկան 7000'=>'10 Մբ/վ 1-ին ամիս 1 դրամով + գիշերային կրկնապատիկ արագություն, Wifi սարք հատուկ գնով');

        $db = oracleConnect();
        $results = "";
        // First, check that the specified field exists.  Very important as we are using other unsanitized data in the following query.
        $query = "SELECT
OS_BILL.CONTRACT_CLASSES.NAME,
OS_BILL.TARIFF_PLANS.PRINT_NAME,
OS_BILL.PERIODIC_SERVE_HISTORY.PRICE,
OS_BILL.PERIODIC_SERVES.PERIODIC_SERVE_ID,
OS_BILL.TARIFF_PLANS.TARIFF_PLAN_ID
FROM
OS_BILL.CONTRACT_CLASSES
INNER JOIN OS_BILL.TARIFF_PLANS ON OS_BILL.TARIFF_PLANS.CONTRACT_CLASS_ID = OS_BILL.CONTRACT_CLASSES.CONTRACT_CLASS_ID
INNER JOIN OS_BILL.PERIODIC_SERVES ON OS_BILL.PERIODIC_SERVES.TARIFF_PLAN_ID = OS_BILL.TARIFF_PLANS.TARIFF_PLAN_ID
INNER JOIN OS_BILL.PERIODIC_SERVE_HISTORY ON OS_BILL.PERIODIC_SERVE_HISTORY.PERIODIC_SERVE_ID = OS_BILL.PERIODIC_SERVES.PERIODIC_SERVE_ID
WHERE
OS_BILL.TARIFF_PLANS.CONNECTION_END_DATE IS NULL AND
OS_BILL.TARIFF_PLANS.CLIENT_TYPE_ACCESS = 1 AND
OS_BILL.PERIODIC_SERVE_HISTORY.CONTRACT_ID IS NULL AND
OS_BILL.PERIODIC_SERVE_HISTORY.END_DATE IS NULL AND
OS_BILL.PERIODIC_SERVE_HISTORY.PRICE <> 0 AND
OS_BILL.PERIODIC_SERVE_HISTORY.PRICE <> '1500' AND
OS_BILL.TARIFF_PLANS.PRINT_NAME IN ('Ռինտերնետ տարեկան 10000', 'Ռինտերնետ տարեկան 6000', 'Ռինտերնետ տարեկան 7500', 'Ռինտերնետ տարեկան 7000')
ORDER BY
OS_BILL.PERIODIC_SERVE_HISTORY.PRICE ASC
";
        $stmt = oci_parse($db, $query);
                    oci_execute($stmt);
                    while (oci_fetch($stmt)) {
                        $tpname = oci_result($stmt, 'PRINT_NAME');
                        $price = oci_result($stmt, 'PRICE');
                        $tpid = oci_result($stmt, 'TARIFF_PLAN_ID');
                        if (array_key_exists($tpname, $tpdesc)) {
                            $desc = $tpdesc[$tpname];
                        }
                        
                        $results .= "<label>
                                <section>
                                    <input type='radio' name='internetradio' id='radiointernet' value='1 $tpid'>
                                    <p>$tpname</p>
                                    <p>$desc</p>
                                    <section class='price'><p>$price դր․/ամիս</p></section>
                                </section>
                            </label>";
}
            
            $results .= "<span class='terms_link'>Մանրամասն պայմաններին կարող եք ծանոթանալ <a href='http://www.rtarmenia.am/am/home/home-internet/rinternet/' style = 'color:rgb(143, 197, 71)' target='_blank'>այս հղումով</a></span>"
                    . "<button id='internet_cancel' type='button'>Չեղարկել</button>";

        $stmt = null;
        return $results;
}


function CheckAvailability($address){
    $result = array('av'=>'', 'htmls'=>'');
    
    $db = oracleConnectRTOSS();
    $dbEM = oracleConnect();
    $firstquery = "SELECT\n" .
            "	OS_BILL.ADDRESSES_REF_VIEW.ADDRESS_ID,\n" .
            "	OS_BILL.ADDRESSES_REF_VIEW.FULL_PATH\n" .
            "FROM\n" .
            "	OS_BILL.ADDRESSES_REF_VIEW\n" .
            "WHERE\n" .
            "	OS_BILL.ADDRESSES_REF_VIEW.FULL_PATH = $address";
    $stmt = oci_parse($db, $firstquery);
    oci_execute($stmt);
    oci_fetch($stmt);
    $address_id = oci_result($stmt, 'ADDRESS_ID');
    //print_r($firstquery);

    if(isset($address_id)) {
        $secondquery = "select decode(nvl(max(AVAILABLE_FOR_CONNECT),
                  '0'),
              '1',
              'Connect',
              '0',
              'Disconnect') as av from OS_BILL.ADDRESSES_REF_M_VIEW where address_id = $address_id";
        $stmt1 = oci_parse($dbEM, $secondquery);
        oci_execute($stmt1);
        oci_fetch($stmt1);
        $av = oci_result($stmt1, 'AV');

        oci_free_statement($stmt);
        oci_free_statement($stmt1);
        oci_close($db);

        if ($av == 'Disconnect') {
            $result['av'] = 'Disconnect';
            $result['htmls'] = ' <div class="col-md-12"><img src="img/av-stat-icons-not.png" alt="..." class="img-square"><div class="col-md-12" id="availability_txt"><h1>ՀԱՍԱՆԵԼԻ ՉԵՆՔ</h1></div>';
            return json_encode($result);
        } else if ($av == 'Connect') {
            $result['av'] = 'Connect';
            $result['htmls'] = '';
            return json_encode($result);
        }
    }
    else{
        return 0;
    }
}

function CheckConnectedApp($address){
    $result = array();
    $address = str_replace("'", "", $address);
    $address = $address." ,";

    $db = oracleConnectRTOSS();
    $dbEM = oracleConnect();
    $firstquery = "SELECT DISTINCT astr_utils.convert_address_kladr(OS_BILL.CONTRACTS_PARAMS_LINK.VALUE) AS USLUG_ADDR
FROM
OS_BILL.CONTRACTS
INNER JOIN OS_BILL.CONTRACT_TARIFF_PLANS ON OS_BILL.CONTRACT_TARIFF_PLANS.CONTRACT_ID = OS_BILL.CONTRACTS.CONTRACT_ID
INNER JOIN OS_BILL.TARIFF_PLANS ON OS_BILL.CONTRACT_TARIFF_PLANS.TARIFF_PLAN_ID = OS_BILL.TARIFF_PLANS.TARIFF_PLAN_ID
INNER JOIN OS_BILL.BILL_CLIENTS_VIEW ON OS_BILL.CONTRACTS.CLIENT_ID = OS_BILL.BILL_CLIENTS_VIEW.CLIENT_ID
LEFT JOIN OS_BILL.NOTIFICATIONS_CLIENT ON OS_BILL.CONTRACTS.CLIENT_ID = OS_BILL.NOTIFICATIONS_CLIENT.CLIENT_ID AND OS_BILL.CONTRACTS.CLIENT_ID = OS_BILL.NOTIFICATIONS_CLIENT.CLIENT_ID
INNER JOIN OS_BILL.CONTRACTS_PARAMS_LINK ON OS_BILL.CONTRACTS_PARAMS_LINK.CONTRACT_ID = OS_BILL.CONTRACTS.CONTRACT_ID
WHERE
astr_utils.convert_address_kladr(OS_BILL.CONTRACTS_PARAMS_LINK.VALUE) LIKE '%$address%' AND
OS_BILL.NOTIFICATIONS_CLIENT.NOTIFICATIONS_CLIENT_TYPE_ID = '27' AND
OS_BILL.CONTRACTS_PARAMS_LINK.CONTRACT_PARAMETER_ID = 1 and ROWNUM <=1000";
    $stmt = oci_parse($dbEM, $firstquery);
    oci_execute($stmt);
    //print_r($address);
    while(oci_fetch($stmt))
    {
        $apt = oci_result($stmt, 'USLUG_ADDR');
        $matches = null;
        $returnValue = preg_match('/ ,(.*)$/', $apt, $matches);

        array_push($result, trim($matches[1]));
        /*$result['contract'] = oci_result($stmt, 'CONTRACT_ID');
        $result['apt'] = oci_result($stmt, 'USLUG_ADDR');*/
    }
    //$address_id = GetAddressID($address);
    //array_push($result, trim($address_id));
    return json_encode($result);

}


function GetAddressID($address){
    
    $db = oracleConnectRTOSS();
    $firstquery = "SELECT\n" .
            "	OS_BILL.ADDRESSES_REF_VIEW.ADDRESS_ID,\n" .
            "	OS_BILL.ADDRESSES_REF_VIEW.FULL_PATH\n" .
            "FROM\n" .
            "	OS_BILL.ADDRESSES_REF_VIEW\n" .
            "WHERE\n" .
            "	OS_BILL.ADDRESSES_REF_VIEW.FULL_PATH = $address";
    //print_r($address);
    $stmt = oci_parse($db, $firstquery);
    oci_execute($stmt);
    oci_fetch($stmt);
    $address_id = oci_result($stmt, 'ADDRESS_ID');
    
    oci_free_statement($stmt);
    oci_close($db);
    
    return $address_id;
}

function CreateDoc($name, $surname, $lastname, $services, $tariffs, $mob, $passsn, $addressID, $address, $bd, $passfrom, $passvalid, $regaddress, $liveaddress, $elhasce, $bnakaran){
    
    $db = oracleConnectRTOSS();
    $servicesList = implode(',', $services);
    $tariffsList = implode(',', $tariffs);
    $address = stripslashes($address);
    $firstquery = "begin
  os_usr.doc_adm_procedures_utl_kosh.p_add_PC_doc_in_queue_from_WEB(pr_first_name                => '$name',
                                                              pr_LAST_NAME                 => '$surname',
                                                              pr_otchestvo                 => '$lastname',
                                                              pr_PHONE                     => '$mob',
                                                              pr_availability              => '0',
                                                              pr_ADDRESS_CONNECT_STR       => $address,
                                                              pr_ADDRESS_CONNECT           => $addressID /*id адреса из справочника*/,
                                                              pr_flat                      => '$bnakaran',
                                                              pr_KANAL_POLUCHENIYA_ZAYAVKI => 463 /*с сайта*/,
                                                              pr_PASPORT_SERIJNYJ_NOMER    => '$passsn',
                                                                  pr_DATA_ROZHDENIYA    => '$bd',
                                                                      pr_PASPORT_KEM_VYDAN    => '$passfrom',
                                                                          pr_PASPORT_DEJSTVUET_DO    => '$passvalid',
                                                                              pr_ADRES_REGISTRACII    => '$regaddress',
                                                                                  pr_ADRES_MESTA_ZHITELSTVA    => '$liveaddress',
                                                                                      pr_E_MAIL    => '$elhasce',
                                                              pr_services                  => adt_integer_table(".$servicesList."),
                                                              pr_tariff_plans              => adt_integer_table(".$tariffsList."));
end;
";

    $stmt = oci_parse($db, $firstquery);
    oci_execute($stmt);
    /*oci_fetch($stmt);
    $address_id = oci_result($stmt, 'ADDRESS_ID');
    //print_r(oci_fetch_assoc($stmt));*/
    
    oci_free_statement($stmt);
    oci_close($db);
    
    echo $firstquery;
}


function CreateSemiDoc($name, $surname, $lastname, $services, $tariffs, $mob, $passsn, $addressID, $address, $bd, $passfrom, $passvalid, $regaddress, $liveaddress, $elhasce, $bnakaran){
    
    $db = oracleConnectRTOSS();
    $servicesList = implode(',', $services);
    $tariffsList = implode(',', $tariffs);
    $address = stripslashes($address);
    $firstquery = "begin
  os_usr.doc_adm_procedures_utl_kosh.p_add_PC_doc_in_queue_from_WEB(pr_first_name                => '$name',
                                                              pr_LAST_NAME                 => '$surname',
                                                              pr_otchestvo                 => '$lastname',
                                                              pr_PHONE                     => '$mob',
                                                              pr_availability              => '1',
                                                              pr_ADDRESS_CONNECT_STR       => $address,
                                                              pr_ADDRESS_CONNECT           => $addressID /*id адреса из справочника*/,
                                                              pr_flat                      => '$bnakaran',
                                                              pr_KANAL_POLUCHENIYA_ZAYAVKI => 463 /*с сайта*/,
                                                              pr_PASPORT_SERIJNYJ_NOMER    => '$passsn',
                                                                  pr_DATA_ROZHDENIYA    => '$bd',
                                                                      pr_PASPORT_KEM_VYDAN    => '$passfrom',
                                                                          pr_PASPORT_DEJSTVUET_DO    => '$passvalid',
                                                                              pr_ADRES_REGISTRACII    => '$regaddress',
                                                                                  pr_ADRES_MESTA_ZHITELSTVA    => '$liveaddress',
                                                                                      pr_E_MAIL    => '$elhasce',
                                                              pr_services                  => adt_integer_table(".$servicesList."),
                                                              pr_tariff_plans              => adt_integer_table(".$tariffsList."));
end;
";

    $stmt = oci_parse($db, $firstquery);
    oci_execute($stmt);
    /*oci_fetch($stmt);
    $address_id = oci_result($stmt, 'ADDRESS_ID');
    //print_r(oci_fetch_assoc($stmt));*/
    
    oci_free_statement($stmt);
    oci_close($db);
    
    echo $firstquery;
}








function fetchAllServices($contractID){

    $db = oracleConnect();
    $results = "";
    // First, check that the specified field exists.  Very important as we are using other unsanitized data in the following query.
    $query = "SELECT
OS_BILL.CONTRACTS.CONTRACT_ID,
OS_BILL.CONTRACTS.START_DATE,
OS_BILL.CONTRACTS.END_DATE,
OS_BILL.CONTRACTS.NAME AS service_name,
astr_utils.convert_address_kladr(OS_BILL.CONTRACTS_PARAMS_LINK.VALUE) AS USLUG_ADDR
FROM
	OS_BILL.CONTRACTS
INNER JOIN OS_BILL.CONTRACTS_PARAMS_LINK ON OS_BILL.CONTRACTS_PARAMS_LINK.CONTRACT_ID = OS_BILL.CONTRACTS.CONTRACT_ID
WHERE
	NAME LIKE '%$contractID%' and OS_BILL.CONTRACTS_PARAMS_LINK.CONTRACT_PARAMETER_ID = 1
";
    $stmt = oci_parse($db, $query);
    oci_execute($stmt);
    while (oci_fetch($stmt)) {
        $contract_id = oci_result($stmt, 'CONTRACT_ID');
        $start_date = oci_result($stmt, 'START_DATE');
        $ned_date = oci_result($stmt, 'END_DATE');
        $service_name = oci_result($stmt, 'SERVICE_NAME');
        $uslug_addr = oci_result($stmt, 'USLUG_ADDR');


        $results .= "<tr>
<td class='center'>$contract_id</td>
<td class='hidden-xs'>$service_name</td>
<td>$start_date</td>
<td>$ned_date</td>
<td class='hidden-xs'>$uslug_addr</td>
</tr>";
    }

    $stmt = null;
    return $results;
}


function setNewAddress($contractID, $addressID, $bnakaran){

    $db = oracleConnectCanChange();
    $results = "";
    // First, check that the specified field exists.  Very important as we are using other unsanitized data in the following query.
    $query = "merge into os_bill.contracts_params_link cpl
using (select 1 as param_id
             ,$contractID as contr_id
             ,OS_BILL.bill_address_api.f_format_address(null,
                                                        null,
                                                        null,
                                                        null,
                                                        '$bnakaran',
                                                        null,
                                                        null,
                                                        '$addressID') as addr
         from dual) s
on (cpl.contract_id = s.contr_id and cpl.contract_parameter_id = s.param_id)
when matched then
  update set cpl.value = s.addr
";
    $stmt = oci_parse($db, $query);
    if(oci_execute($stmt))
    {
        $results .= "Հասցեն ուղղված է։";
    }


    oci_free_statement($stmt);
    oci_close($db);
    return $results;
}


function quote($str) {
    return sprintf("'%s'", $str);
}

function array2csv(array &$array)
{
    if (count($array) == 0) {
        return null;
    }
    ob_start();
    $df = fopen("php://output", 'w');
    fputcsv($df, array_keys(reset($array)));
    foreach ($array as $row) {
        fputcsv($df, $row);
    }
    fclose($df);
    return ob_get_clean();
}

function download_send_headers($filename) {
    // disable caching
    $now = gmdate("D, d M Y H:i:s");
    header("Expires: Tue, 03 Jul 2021 06:00:00 GMT");
    header("Cache-Control: max-age=0, no-cache, must-revalidate, proxy-revalidate");
    header("Last-Modified: {$now} GMT");

    // force download
    header("Content-Type: application/force-download");
    header("Content-Type: application/octet-stream");
    header("Content-Type: application/download");

    // disposition / encoding on response body
    header("Content-Disposition: attachment;filename={$filename}");
    header("Content-Transfer-Encoding: binary");
}

function fetchPartqiCucak($contractIDList, $balanceDate){

    $returnValue = preg_split('\',\'', $contractIDList, -1);
    $returnValue=array_map('trim',$returnValue);

    $contractList = implode(',', array_map('quote', $returnValue));
    $rootContractID = 0;


    $db = oracleConnectCanChange();
    $emDB = oracleConnectRTOSS();

    $results = "                        <table id=\"example\" class=\"display\" cellspacing=\"0\" width=\"100%\">
                            <thead>
                            <tr>
                                <th>Պայմանագրի համար</th>
                                <th>Անուն</th>
                                <th>Հայրանուն</th>
                                <th>Ազգանուն</th>
                                <th>Անձի տիպ</th>
                                <th>ՀՎՀՀ</th>
                                <th>Անձնագրի համար</th>
                                <th>Անձնագրի տրման ամսաթիվ</th>
                                <th>Անձնագիր/ում կողմից</th>
                                <th>Ծննդյան ամսաթիվ</th>
                                <th>Նույնականացման քարտ</th>
                                <th>Բնակության հասցե</th>
                                <th>Գրանցման հասցե</th>
                                <th>Պայմանագրի ամսաթիվ</th>
                                <th>Լուծման ամսաթիվ</th>
                                <th>Ծառայության անվանում</th>
                                <th>Ֆիքսված հեռախոսակապի հեռախոսահամար</th>
                                <th>Սարքավորման անվանում INET</th>
                                <th>Սարքավորման անվանում IPTV</th>
                                <th>Հաշվարկի ամսաթիվ</th>
                                <th>Բաժանորդային պայմանագրի հաշվեկշիռ բիլինգային համակարգում</th>
                                <th>Հրաժարագին</th>
                                <th>Բիլլինգային հաշվեկշիռ առանց տույժերի</th>
                                <th>Ժամկետանց պարտավորության նկատմամբ հաշվարկված տույժ</th>
                                <th>Ընդամենը հայցման գումար</th>
                                <th>Կոնտակտ</th>
                                <th>Դիմումի ստորագրման ամսաթիվ</th>


                            </tr>
                            </thead>
                            <tbody id=\"partqi-cucak1\">


";


    // First, check that the specified field exists.  Very important as we are using other unsanitized data in the following query.
    $query = "SELECT
OS_BILL.CONTRACTS.END_DATE,
OS_BILL.CONTRACTS.CLIENT_ID,
OS_BILL.CONTRACTS.CONTRACT_ID,
OS_BILL.CONTRACTS.NAME,
OS_BILL.BILL_CLIENTS_VIEW.FIRST_NAME,
OS_BILL.BILL_CLIENTS_VIEW.MIDDLE_NAME,
OS_BILL.BILL_CLIENTS_VIEW.LAST_NAME,
OS_BILL.CLIENT_TYPES.NAME AS ANDZITIP,
OS_BILL.BILL_CLIENTS_VIEW.PASSPORT,
OS_BILL.BILL_CLIENTS_VIEW.PASSPORT_DATE,
OS_BILL.BILL_CLIENTS_VIEW.DATE_EXPIRY,
OS_BILL.BILL_CLIENTS_VIEW.DRAW_PASSPORT,
OS_BILL.BILL_CLIENTS_VIEW.BIRTH_DATE,
astr_utils.convert_address_kladr(OS_BILL.BILL_CLIENTS_VIEW.ADDRESS) AS BNAK_ADDR,
astr_utils.convert_address_kladr(OS_BILL.BILL_CLIENTS_VIEW.REGISTRATION_ADDRES) AS REGISTRATION_ADDRESS,
OS_BILL.CONTRACTS.START_DATE,
OS_BILL.CONTRACTS.CONTRACT_ID,
OS_BILL.NOTIFICATIONS_CLIENT.VALUE as mobile
FROM
OS_BILL.CONTRACTS
INNER JOIN OS_BILL.BILL_CLIENTS_VIEW ON OS_BILL.CONTRACTS.CLIENT_ID = OS_BILL.BILL_CLIENTS_VIEW.CLIENT_ID
INNER JOIN OS_BILL.CLIENT_TYPES ON OS_BILL.BILL_CLIENTS_VIEW.PHYSICAL = OS_BILL.CLIENT_TYPES.CLIENT_TYPE_ID
LEFT JOIN OS_BILL.NOTIFICATIONS_CLIENT ON OS_BILL.CONTRACTS.CLIENT_ID = OS_BILL.NOTIFICATIONS_CLIENT.CLIENT_ID --AND OS_BILL.CONTRACTS.CLIENT_ID = OS_BILL.NOTIFICATIONS_CLIENT.CLIENT_ID

WHERE
RTRIM(OS_BILL.CONTRACTS.CODE) in ($contractList) --and OS_BILL.NOTIFICATIONS_CLIENT.NOTIFICATIONS_CLIENT_TYPE_ID = '27' AND
--OS_BILL.NOTIFICATIONS_CLIENT.IS_DELETED IS NULL
";
    $stmt = oci_parse($db, $query);
    if(oci_execute($stmt))
    {


        while (oci_fetch($stmt)) {
            $client_id = oci_result($stmt, 'CLIENT_ID');
            $client_contractID = oci_result($stmt, 'CONTRACT_ID');
            $contract_id = oci_result($stmt, 'NAME');
            $first_name = oci_result($stmt, 'FIRST_NAME');
            $middle_name = oci_result($stmt, 'MIDDLE_NAME');
            $last_name = oci_result($stmt, 'LAST_NAME');
            $andzi_tip = oci_result($stmt, 'ANDZITIP');
            $passport = oci_result($stmt, 'PASSPORT');
            $passport_date = oci_result($stmt, 'PASSPORT_DATE');
            $passport_expiry = oci_result($stmt, 'DATE_EXPIRY');
            $draw_passport = oci_result($stmt, 'DRAW_PASSPORT');
            $bnak_addr = oci_result($stmt, 'BNAK_ADDR');
            $reg_addr = oci_result($stmt, 'REGISTRATION_ADDRESS');
            $start_date = oci_result($stmt, 'START_DATE');
            $end_date = oci_result($stmt, 'END_DATE');
            $rootContractID = oci_result($stmt, 'CONTRACT_ID');
            $mobile = oci_result($stmt, 'MOBILE');
            $dob = oci_result($stmt, 'BIRTH_DATE');

            $fetchServicesQueryStr = "SELECT
OS_BILL.CONTRACTS.CODE
FROM
OS_BILL.CONTRACTS
WHERE
OS_BILL.CONTRACTS.CLIENT_ID = $client_id AND
OS_BILL.CONTRACTS.CONTRACT_TYPE_ID = 3";
            $services = array();
            $service_list = '';

            $stmt2 = oci_parse($db, $fetchServicesQueryStr);
            if(oci_execute($stmt2))
            {
                while (oci_fetch($stmt2)) {
                    $service = oci_result($stmt2, 'CODE');
                    array_push($services, $service);
                }
                $service_list = implode(',', $services);
            }


            //get resources list
            $fetchResourcesQueryStr = "SELECT
OS_BILL.CONTRACTS.CONTRACT_ID,
OS_BILL.CONTRACTS.CLIENT_ID,
OS_BILL.CONTRACTS.CODE,
OS_BILL.RESOURCES.RESOURCE_TYPE_ID,
OS_BILL.RESOURCES.VALUE,
OS_BILL.RESOURCES.RESOURCE_ID,
OS_BILL.RESOURCE_PARAMS_VALUES.PARAM_VALUE,
OS_BILL.RESOURCE_HISTORY.END_DATE
FROM
OS_BILL.CONTRACTS
INNER JOIN OS_BILL.RESOURCE_HISTORY ON OS_BILL.RESOURCE_HISTORY.CONTRACT_ID = OS_BILL.CONTRACTS.CONTRACT_ID
INNER JOIN OS_BILL.RESOURCES ON OS_BILL.RESOURCE_HISTORY.RESOURCE_ID = OS_BILL.RESOURCES.RESOURCE_ID
FULL OUTER JOIN OS_BILL.RESOURCE_PARAMS_VALUES ON OS_BILL.RESOURCE_PARAMS_VALUES.RESOURCE_ID = OS_BILL.RESOURCES.RESOURCE_ID AND
OS_BILL.RESOURCE_PARAMS_VALUES.PARAM_ID = 3
WHERE
OS_BILL.CONTRACTS.NAME LIKE '%$contract_id%' AND
OS_BILL.CONTRACTS.CONTRACT_TYPE_ID = 3 AND
OS_BILL.CONTRACTS.CODE <> 'WEB' AND
OS_BILL.RESOURCES.RESOURCE_TYPE_ID <> 19721
";
            $fix_tels = array();
            $tel_list = '';

            $inet_resources = array();
            $inet_list = '';

            $tv_resources = array();
            $tv_list = '';

            $stmt4 = oci_parse($db, $fetchResourcesQueryStr);
            if(oci_execute($stmt4))
            {
                while (oci_fetch($stmt4)) {
                    $resourece_type = oci_result($stmt4, 'RESOURCE_TYPE_ID');
                    switch($resourece_type){
                        case 19002:
                            $fix_tel = oci_result($stmt4, 'VALUE');
                            array_push($fix_tels, $fix_tel);
                            break;
                        case 19013:
                        case 19014:
                            $inet_resource = oci_result($stmt4, 'PARAM_VALUE');
                            array_push($inet_resources, $inet_resource);
                            break;
                        case 19714:
                        case 19720:
                        case 19722:
                            $tv_resource = oci_result($stmt4, 'PARAM_VALUE');
                            array_push($tv_resources, $tv_resource);
                            break;

                    }

                }
                $tel_list = implode(',', $fix_tels);
                $inet_list = implode(' ,', $inet_resources);
                $tv_list = implode(' ,', $tv_resources);
            }




            //get fines data
            $fetchFinesQueryStr = "SELECT
OS_BILL.SINGLE_PAYMENT_VIEW.CLIENT_ID,
OS_BILL.SINGLE_PAYMENT_VIEW.CONTRACT_ID,
OS_BILL.SINGLE_PAYMENT_VIEW.PAYMENT,
OS_BILL.SINGLE_PAYMENT_VIEW.SINGLE_SERVE_ID
FROM
OS_BILL.SINGLE_PAYMENT_VIEW
INNER JOIN OS_BILL.CONTRACTS ON OS_BILL.CONTRACTS.CLIENT_ID = OS_BILL.SINGLE_PAYMENT_VIEW.CLIENT_ID
WHERE
OS_BILL.CONTRACTS.CONTRACT_ID = '$client_contractID' AND
OS_BILL.SINGLE_PAYMENT_VIEW.SINGLE_SERVE_ID = 208
";

            $tugank = 0;
            $stmt5 = oci_parse($db, $fetchFinesQueryStr);
            if(oci_execute($stmt5))
            {
                while (oci_fetch($stmt5)) {

                    $tugank = oci_result($stmt5, 'PAYMENT');

                }
            }




            $getBalance = "SELECT to_char(OS_BILL.balances_history_api.f_get_comon_balance_by_date($rootContractID, to_date('$balanceDate', 'DD.MM.YYYY')), '99999999.99') as balance from dual";

            $stmt3 = oci_parse($db, $getBalance);
            if(oci_execute($stmt3))
            {
                while (oci_fetch($stmt3)) {
                    //$balance = number_format(oci_result($stmt3, 'BALANCE'), 2, '.', '');
                    $balance = oci_result($stmt3, 'BALANCE');
                    //$balance = sprintf("%0.2f",$balance);
                    //var_dump($balance);
                    //$balance = number_format($balance);


                }
            }


            //get penalities
            $getPenaltiesQueryStr = "SELECT OS_BILL.PENALTIES.AMOUNT FROM OS_BILL.PENALTIES WHERE CONTRACT_ID = '$client_contractID'";

            $penal = 0;
            $stmt6 = oci_parse($db, $getPenaltiesQueryStr);
            if(oci_execute($stmt6))
            {
                while (oci_fetch($stmt6)) {

                    $penal = oci_result($stmt6, 'AMOUNT');

                }
            }

            if($balance < 0) {
                $balance_wo_penal = $balance + $tugank + $penal;
            }
            else
            {
                if($balance != 0) {
                    $balance_wo_penal = abs($balance) - $tugank - $penal;
                }
                else{
                    $balance_wo_penal = 0;
                }
            }


            //get contract sign date
            $getSignDate = "select os_eqm.documents_4.DATE_SIGNING_CONTRACT from os_eqm.documents_4 WHERE contract_name = '$contract_id'";
            $sign_date = NULL;

            $stmt7 = oci_parse($emDB, $getSignDate) or die(oci_error($db));
            //$response = oci_execute($stmt7) or die(oci_error($db));
            if(oci_execute($stmt7))
            {

                while (oci_fetch($stmt7)) {

                    $sign_date = oci_result($stmt7, 'DATE_SIGNING_CONTRACT');



                }
            }
















            $results .= "<tr>
<td>$contract_id</td>
<td>$first_name</td>
<td>$middle_name</td>
<td>$last_name</td>
<td>$andzi_tip</td>
<td></td>
<td>$passport</td>
<td>$passport_date</td>
<td>$draw_passport</td>
<td>$dob</td>
<td></td>
<td>$bnak_addr</td>
<td>$reg_addr</td>
<td>$start_date</td>
<td>$end_date</td>
<td>$service_list</td>
<td>$tel_list</td>
<td>$inet_list</td>
<td>$tv_list</td>
<td>$balanceDate</td>
<td>$balance</td>
<td>$tugank</td>
<td>$balance_wo_penal</td>
<td>$penal</td>
<td></td>
<td>$mobile</td>
<td>$sign_date</td>





</tr>";
        }
        $results .= "                            </tbody>
                        </table>";
    }


    oci_free_statement($stmt);
    oci_close($db);
    return $results;
}