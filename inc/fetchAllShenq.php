<?php

/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 * Easy set variables
 */

// DB table to use
$table = 'shenq';

// Table's primary key
$primaryKey = 'address_id';

// Array of database columns which should be read and sent back to DataTables.
// The `db` parameter represents the column name in the database, while the `dt`
// parameter represents the DataTables column identifier. In this case simple
// indexes
$columns = array(
    array( 'db' => 'edit', 'dt' => 0 ), //address_id
    array( 'db' => 'address_string',  'dt' => 1 ),
    array( 'db' => 'city',  'dt' => 2 ),
    array( 'db' => 'region',  'dt' => 3 ),
    array( 'db' => 'street',  'dt' => 4 ),
    array( 'db' => 'sub_street',  'dt' => 5 ),
    array( 'db' => 'shenq',  'dt' => 6 ),
    array( 'db' => 'mutqeri_qanak',   'dt' => 7 ),
    array( 'db' => 'harkeri_tiv',     'dt' => 8 ),
    array( 'db' => 'bnakaranneri_tiv',     'dt' => 9 ),
    array( 'db' => 'tup',     'dt' => 10 ),
    array( 'db' => 'comments',     'dt' => 11 )
);

// SQL server connection information
$sql_details = array(
    'user' => 'sevada',
    'pass' => 'AnkappasS123',
    'db'   => 'd2d',
    'host' => '192.168.1.176'
);


/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 * If you just want to use the basic configuration for DataTables with PHP
 * server-side, there is no need to edit below this line.
 */

require( 'ssp.class.php' );

echo json_encode(
    SSP::simple( $_GET, $sql_details, $table, $primaryKey, $columns )
);