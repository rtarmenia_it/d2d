<?php
require_once 'db-settings.php';
require_once 'oracle-db-settings.php';
/*****************  Region fetch data functions *******************/

function fetchAllRegions(){
    try {
        global $db_table_prefix;

        $results = '';

        //$db = pdoConnect();
        $db = oracleConnectRTOSS();

        $sqlVars = array();

        $query = "select * from {$db_table_prefix}region_ml where lng_id=3";

        $stmt = $db->prepare($query);
        $stmt->execute($sqlVars);
        $row = $stmt->fetchAll(PDO::FETCH_ASSOC);
        foreach ($row as $key => $value) {
            $results .= "<option value="."\"".$value['label']."\" data-id=".$value['id'].">".$value['label']."</option>";
        }

        $stmt = null;
        return $results;

    } catch (PDOException $e) {
        
        error_log("Error in " . $e->getFile() . " on line " . $e->getLine() . ": " . $e->getMessage());
        return false;
    } catch (ErrorException $e) {
      
      return false;
    }
}

function fetchAllCity($region){
    try {
        global $db_table_prefix;

        $results = '<option value="0">Ընտրեք քաղաքը</option>';

        $db = pdoConnect();

        $sqlVars = array();

        $query = "SELECT
	`s1_city`.`id` AS `key`,
	`label` AS `value`
FROM
	`s1_city`
INNER JOIN s1_city_ml ON (
	`s1_city`.`id` = `s1_city_ml`.`id`
	AND `s1_city_ml`.`lng_id` = 3
)
WHERE
	`active`
AND `region` = '$region'
AND `lng_id` = '3'
ORDER BY
	`label` ASC";

        $stmt = $db->prepare($query);
        $stmt->execute($sqlVars);
        $row = $stmt->fetchAll(PDO::FETCH_ASSOC);
        foreach ($row as $key => $value) {
            $results .= "<option value="."\"".$value['value']."\" data-id=".$value['key'].">".$value['value']."</option>";
        }

        $stmt = null;
        return $results;

    } catch (PDOException $e) {
        
        error_log("Error in " . $e->getFile() . " on line " . $e->getLine() . ": " . $e->getMessage());
        return false;
    } catch (ErrorException $e) {
      
      return false;
    }
}



function fetchAllDistrict($city){
    try {
        global $db_table_prefix;

        $results = '<option value="0">Ընտրեք համայնքը</option>';

        $db = pdoConnect();

        $sqlVars = array();

        $query = "SELECT
	`s1_district`.`id` AS `key`,
	`label` AS `value`
FROM
	`s1_district`
INNER JOIN s1_district_ml ON (
	`s1_district`.`id` = `s1_district_ml`.`id`
	AND `s1_district_ml`.`lng_id` = 3
)
WHERE
	`active`
AND `city` = '$city'
AND `lng_id` = '3'
ORDER BY
	`label` ASC";

        $stmt = $db->prepare($query);
        $stmt->execute($sqlVars);
        $row = $stmt->fetchAll(PDO::FETCH_ASSOC);
        foreach ($row as $key => $value) {
            $results .= "<option value="."\"".$value['value']."\" data-id=".$value['key'].">".$value['value']."</option>";
        }

        $stmt = null;
        return $results;

    } catch (PDOException $e) {
        
        error_log("Error in " . $e->getFile() . " on line " . $e->getLine() . ": " . $e->getMessage());
        return false;
    } catch (ErrorException $e) {
      
      return false;
    }
}


function fetchAllStreets($district){
    try {
        global $db_table_prefix;

        $results = '<option value="0">Ընտրեք փողոցը</option>';

        $db = pdoConnect();

        $sqlVars = array();

        $query = "SELECT
	`s1_street`.`id` AS `key`,
	`label` AS `value`
FROM
	`s1_street`
INNER JOIN s1_street_ml ON (
	`s1_street`.`id` = `s1_street_ml`.`id`
	AND `s1_street_ml`.`lng_id` = 3
)
WHERE
	`active`
AND `district` = '$district'
AND `lng_id` = '3'
ORDER BY
	`label` ASC";

        $stmt = $db->prepare($query);
        $stmt->execute($sqlVars);
        $row = $stmt->fetchAll(PDO::FETCH_ASSOC);
        foreach ($row as $key => $value) {
            $results .= "<option value="."\"".$value['value']."\""." data-id=".$value['key'].">".$value['value']."</option>";
        }

        $stmt = null;
        return $results;

    } catch (PDOException $e) {
        
        error_log("Error in " . $e->getFile() . " on line " . $e->getLine() . ": " . $e->getMessage());
        return false;
    } catch (ErrorException $e) {
      
      return false;
    }
}


function fetchAllHouses($street){
    try {
        global $db_table_prefix;

        $results = '<option value="0">Ընտրեք շենքը</option>';

        $db = pdoConnect();

        $sqlVars = array();

        $query = "SELECT
	`s1_house`.`id` AS `key`,
	`label` AS `value`
FROM
	`s1_house`
INNER JOIN s1_house_ml ON (
	`s1_house`.`id` = `s1_house_ml`.`id`
	AND `s1_house_ml`.`lng_id` = 3
)
WHERE
	`active`
AND `street` = '$street'
AND `lng_id` = '3'
ORDER BY
	ABS(`label`)";

        $stmt = $db->prepare($query);
        $stmt->execute($sqlVars);
        $row = $stmt->fetchAll(PDO::FETCH_ASSOC);
        foreach ($row as $key => $value) {
            $results .= "<option value="."\"".$value['value']."\" data-id=".$value['key'].">".$value['value']."</option>";
        }

        $stmt = null;
        return $results;

    } catch (PDOException $e) {
        
        error_log("Error in " . $e->getFile() . " on line " . $e->getLine() . ": " . $e->getMessage());
        return false;
    } catch (ErrorException $e) {
      
      return false;
    }
}