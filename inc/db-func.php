<?php
require_once 'db-settings.php';
require_once 'oracle-db-settings.php';
/*****************  Region fetch data functions *******************/

function fetchAllRegions(){
    $db = oracleConnectRTOSS();
    $results = '';
    $res = array();
    $query = "SELECT DISTINCT OS_BILL.ADDRESSES_REF_VIEW.MARZ FROM OS_BILL.ADDRESSES_REF_VIEW
WHERE OS_BILL.ADDRESSES_REF_VIEW.MARZ IS NOT NULL";
    $stmt = oci_parse($db, $query);
    oci_execute($stmt);
    oci_fetch_all($stmt, $res);
    foreach ($res as $col) {
        foreach ($col as $item) {
            $results .= "<option value="."\"".$item."\" data-id=".$item.">".$item."</option>";
        }
    }
    oci_free_statement($stmt);
    oci_close($db);
    
    return $results;
}

function fetchAllCity($region){
    $db = oracleConnectRTOSS();
    $results = '<option value="0">Ընտրեք քաղաքը</option>';
    $res = array();
    $query = "SELECT DISTINCT OS_BILL.ADDRESSES_REF_VIEW.CITY FROM OS_BILL.ADDRESSES_REF_VIEW
WHERE OS_BILL.ADDRESSES_REF_VIEW.MARZ = '$region' ORDER BY OS_BILL.ADDRESSES_REF_VIEW.CITY ASC";
    $stmt = oci_parse($db, $query);
    oci_execute($stmt);
    oci_fetch_all($stmt, $res);
    foreach ($res as $col) {
        foreach ($col as $item) {
            $results .= "<option value="."\"".$item."\" data-id=".$item.">".$item."</option>";
        }
    }
    oci_free_statement($stmt);
    oci_close($db);
    
    return $results;
}



function fetchAllDistrict($city){
    $db = oracleConnectRTOSS();
    $results = '<option value="0">Ընտրեք համայնքը</option>';
    $res = array();
    $query = "SELECT DISTINCT OS_BILL.ADDRESSES_REF_VIEW.DISTRICT FROM OS_BILL.ADDRESSES_REF_VIEW
WHERE OS_BILL.ADDRESSES_REF_VIEW.CITY = '$city' ORDER BY OS_BILL.ADDRESSES_REF_VIEW.DISTRICT ASC";
    $stmt = oci_parse($db, $query);
    oci_execute($stmt);
    oci_fetch_all($stmt, $res);
    foreach ($res as $col) {
        foreach ($col as $item) {
            $results .= "<option value="."\"".$item."\" data-id=".$item.">".$item."</option>";
        }
    }
    oci_free_statement($stmt);
    oci_close($db);
    
    return $results;
}


function fetchAllStreets($district){
    $db = oracleConnectRTOSS();
    $results = '<option value="0">Ընտրեք փողոցը</option>';
    $res = array();
    $query = "SELECT DISTINCT OS_BILL.ADDRESSES_REF_VIEW.STREET FROM OS_BILL.ADDRESSES_REF_VIEW
WHERE OS_BILL.ADDRESSES_REF_VIEW.DISTRICT = '$district' AND OS_BILL.ADDRESSES_REF_VIEW.STREET IS NOT NULL ORDER BY OS_BILL.ADDRESSES_REF_VIEW.STREET ASC";
    $stmt = oci_parse($db, $query);
    oci_execute($stmt);
    oci_fetch_all($stmt, $res);
    foreach ($res as $col) {
        foreach ($col as $item) {
            $results .= "<option value="."\"".$item."\" data-id=".$item.">".$item."</option>";
        }
    }
    oci_free_statement($stmt);
    oci_close($db);
    
    return $results;
}

function fetchAllStreetsSubtype($street, $city){
    $db = oracleConnectRTOSS();
    $results = '<option value="">Ընտրեք նրբանցքը</option><option value="">Նրբանցք չկա</option>';
    $res = array();
    $query = "SELECT DISTINCT OS_BILL.ADDRESSES_REF_VIEW.STREET_SUBTYPE FROM OS_BILL.ADDRESSES_REF_VIEW
WHERE OS_BILL.ADDRESSES_REF_VIEW.STREET = '$street' AND OS_BILL.ADDRESSES_REF_VIEW.STREET_SUBTYPE IS NOT NULL AND OS_BILL.ADDRESSES_REF_VIEW.DISTRICT = '$city'";
    $stmt = oci_parse($db, $query);
    oci_execute($stmt);
    $nrows = oci_fetch_all($stmt, $res);
    if($nrows != 0){
        $results = '<option value="">Ընտրեք նրբանցքը</option>';
    }
    foreach ($res as $col) {
        foreach ($col as $item) {
            $results .= "<option value="."\"".$item."\" data-id=".$item.">".$item."</option>";
        }
    }
    oci_free_statement($stmt);
    oci_close($db);
    
    return $results;
}


function fetchAllHouses($city, $district, $street, $street_subtype){
$db = oracleConnectRTOSS();
    $results = '<option value="">Ընտրեք շենքը</option>';
    $res = array();
    if(empty($street_subtype)){
    $query = "SELECT
OS_BILL.ADDRESSES_REF_VIEW.HOUSES_NUM
FROM OS_BILL.ADDRESSES_REF_VIEW
WHERE
OS_BILL.ADDRESSES_REF_VIEW.STREET = '$street' AND
OS_BILL.ADDRESSES_REF_VIEW.DISTRICT = '$district' AND
OS_BILL.ADDRESSES_REF_VIEW.CITY = '$city' AND
OS_BILL.ADDRESSES_REF_VIEW.STREET_SUBTYPE IS NULL";
    }
 else {
     $query = "SELECT
OS_BILL.ADDRESSES_REF_VIEW.HOUSES_NUM
FROM OS_BILL.ADDRESSES_REF_VIEW
WHERE
OS_BILL.ADDRESSES_REF_VIEW.STREET = '$street' AND
OS_BILL.ADDRESSES_REF_VIEW.DISTRICT = '$district' AND
OS_BILL.ADDRESSES_REF_VIEW.CITY = '$city' AND
OS_BILL.ADDRESSES_REF_VIEW.STREET_SUBTYPE = '$street_subtype'";
    }
    $stmt = oci_parse($db, $query);
    oci_execute($stmt);
    oci_fetch_all($stmt, $res);
    foreach ($res as $col) {
        foreach ($col as $item) {
            $results .= "<option value="."\"".$item."\" data-id=".$item.">".$item."</option>";
        }
    }
    oci_free_statement($stmt);
    oci_close($db);
    
    return $results;
}

function InsertAptData($addressID, $address_o, $apt_hamar, $apt_bnakecvac, $inet_operator, $sak_patet, $sakagin, $contract_end_date, $ayl_nshumner, $comments){
    try {
    $db = pdoConnect();
    $results = '';
        $edit = "<button type='button' class='btn btn-small btn-o btn-primary'id='edit-apt' data-id='$addressID' data-uid='$apt_hamar'>Edit</button>";
        $ayl_nshumner = str_replace('_', ' ' ,$ayl_nshumner);
        //$address = "<button type='button' class='btn btn-wide btn-o btn-primary' id='edit-apt' data-id='$addressID'>$address_o</button>";

        $splitedAddress = explode(',', $address_o);
        $marz = trim($splitedAddress[0]);
        $city = trim($splitedAddress[1]);
        $region = trim($splitedAddress[2]);
        $street = trim($splitedAddress[3]);
        $subStreet = trim($splitedAddress[4]);
        $shenq = trim($splitedAddress[5]);

        if($shenq == null){
            swap($subStreet, $shenq);
        }
        //var_dump($shenq);
        //var_dump($subStreet);
        //$address = "<a href='edit_apt.php?id=$addressID'>$address</a>";

    $query = "INSERT INTO apt(address_id,
            address_str,
            city,
            region,
            street,
            sub_street,
            shenq,
            apt_hamar,
            apt_bnakecvac,
            inet_operator,
            sak_patet,
            sakagin,
            contract_end_date,
            ayl_nshumner,
            apt_comments,
            edit) VALUES (
            :address_id,
            :address_str,
            :city,
            :region,
            :street,
            :sub_street,
            :shenq,
            :apt_hamar,
            :apt_bnakecvac,
            :inet_operator,
            :sak_patet,
            :sakagin,
            :contract_end_date,
            :ayl_nshumner,
            :comments,
            :edit)";

    $stmt = $db->prepare($query);

    $stmt->bindParam(':address_id', $addressID, PDO::PARAM_INT);
    $stmt->bindParam(':address_str', $marz, PDO::PARAM_STR);
        $stmt->bindParam(':city', $city, PDO::PARAM_STR);
        $stmt->bindParam(':region', $region, PDO::PARAM_STR);
        $stmt->bindParam(':street', $street, PDO::PARAM_STR);
        $stmt->bindParam(':sub_street', $subStreet, PDO::PARAM_STR);
        $stmt->bindParam(':shenq', $shenq, PDO::PARAM_STR);
    $stmt->bindParam(':apt_hamar', $apt_hamar, PDO::PARAM_STR);
// use PARAM_STR although a number
    $stmt->bindParam(':apt_bnakecvac', $apt_bnakecvac, PDO::PARAM_STR);
    $stmt->bindParam(':inet_operator', $inet_operator, PDO::PARAM_STR);
    $stmt->bindParam(':sak_patet', $sak_patet, PDO::PARAM_STR);
    $stmt->bindParam(':sakagin', $sakagin, PDO::PARAM_STR);
    $stmt->bindParam(':contract_end_date', $contract_end_date, PDO::PARAM_STR);
    $stmt->bindParam(':ayl_nshumner', $ayl_nshumner, PDO::PARAM_STR);
    $stmt->bindParam(':comments', $comments, PDO::PARAM_STR);
        $stmt->bindParam(':edit', $edit, PDO::PARAM_STR);

    $stmt->execute();
        //$db->commit();
/*        $lid = $db->lastInsertId();
        $query = "UPDATE apt SET address_str = :ad
            WHERE address_id = :address_id AND id = :lid";
        $stmt = $db->prepare($query);

        $address = "<button type='button' class='btn btn-wide btn-o btn-primary' id='edit-apt' data-uid='$lid' data-id='$addressID'>$address_o</button>";
        $stmt->bindParam(':ad', $address, PDO::PARAM_STR);
        $stmt->bindParam(':address_id', $addressID, PDO::PARAM_INT);
        $stmt->bindParam(':lid', $lid, PDO::PARAM_INT);

        $stmt->execute();*/

    echo "Բնակարանի տվյալներն հաջողությամբ ավելացված են:";
        //header("Refresh:0");
} catch (PDOException $e) {
        //$db->rollback();
        error_log("Error in " . $e->getFile() . " on line " . $e->getLine() . ": " . $e->getMessage());
        echo "Տեղի է ունեցել սխալ";
    return false;
} catch (ErrorException $e) {
        echo "Տեղի է ունեցել սխալ";
    return false;
}
}


function UpdateAptData($addressID, $apt_hamar, $old_apt, $apt_bnakecvac, $inet_operator, $sak_patet, $sakagin, $contract_end_date, $ayl_nshumner, $comments){
    try {
        $db = pdoConnect();
        $results = '';
        //$address = "<button type='button' class='btn btn-wide btn-o btn-primary' id='edit-apt' data-apt='$apt_hamar' data-id='$addressID'>$address</button>";
        //$address = "<a href='edit_apt.php?id=$addressID'>$address</a>";

        $ayl_nshumner = str_replace('_', ' ' ,$ayl_nshumner);

        $query = "UPDATE apt SET apt_hamar = :apt_hamar,
            apt_bnakecvac = :apt_bnakecvac,
            inet_operator = :inet_operator,
            sak_patet = :sak_patet,
            sakagin = :sakagin,
            contract_end_date = :contract_end_date,
            ayl_nshumner = :ayl_nshumner,
            apt_comments = :comments
            WHERE address_id = :address_id AND apt_hamar = :apt_old";

        $stmt = $db->prepare($query);

        $stmt->bindParam(':address_id', $addressID, PDO::PARAM_INT);
        $stmt->bindParam(':apt_hamar', $apt_hamar, PDO::PARAM_STR);
        $stmt->bindParam(':apt_old', $old_apt, PDO::PARAM_STR);
// use PARAM_STR although a number
        $stmt->bindParam(':apt_bnakecvac', $apt_bnakecvac, PDO::PARAM_STR);
        $stmt->bindParam(':inet_operator', $inet_operator, PDO::PARAM_STR);
        $stmt->bindParam(':sak_patet', $sak_patet, PDO::PARAM_STR);
        $stmt->bindParam(':sakagin', $sakagin, PDO::PARAM_STR);
        $stmt->bindParam(':contract_end_date', $contract_end_date, PDO::PARAM_STR);
        $stmt->bindParam(':ayl_nshumner', $ayl_nshumner, PDO::PARAM_STR);
        $stmt->bindParam(':comments', $comments, PDO::PARAM_STR);

        $stmt->execute();

        echo "Բնակարանի տվյալներն հաջողությամբ խմբագրված են:";
    } catch (PDOException $e) {

        error_log("Error in " . $e->getFile() . " on line " . $e->getLine() . ": " . $e->getMessage());
        echo "Տեղի է ունեցել սխալ";
        return false;
    } catch (ErrorException $e) {
        echo "Տեղի է ունեցել սխալ";
        return false;
    }
}


/*
 * Delete apt data from DB
 */
function DeleteAptData($addressID, $apt_hamar){
    try {
        $db = pdoConnect();
        $results = '';

        $query = "DELETE FROM apt WHERE address_id = :address_id AND apt_hamar = :apt_hamar";

        $stmt = $db->prepare($query);

        $stmt->bindParam(':address_id', $addressID, PDO::PARAM_INT);
        $stmt->bindParam(':apt_hamar', $apt_hamar, PDO::PARAM_STR);

        $stmt->execute();

        echo "Բնակարանի տվյալներն հաջողությամբ հեռացված են:";
    } catch (PDOException $e) {

        error_log("Error in " . $e->getFile() . " on line " . $e->getLine() . ": " . $e->getMessage());
        echo "Տեղի է ունեցել սխալ";
        return false;
    } catch (ErrorException $e) {
        echo "Տեղի է ունեցել սխալ";
        return false;
    }
}


/***
 * @param $addressID
 * @return bool
 */
function DeleteShenqData($addressID){
    try {
        $db = pdoConnect();
        $results = '';

        $query = "DELETE FROM shenq WHERE address_id = :address_id";

        $stmt = $db->prepare($query);

        $stmt->bindParam(':address_id', $addressID, PDO::PARAM_INT);

        $stmt->execute();

        echo "Շենքի տվյալներն հաջողությամբ հեռացված են:";
    } catch (PDOException $e) {

        error_log("Error in " . $e->getFile() . " on line " . $e->getLine() . ": " . $e->getMessage());
        echo "Տեղի է ունեցել սխալ";
        return false;
    } catch (ErrorException $e) {
        echo "Տեղի է ունեցել սխալ";
        return false;
    }
}





function InsertShenqData($addressID, $address, $mutq, $hark, $bnakaran, $tup, $comments){
    try {
        $db = pdoConnect();
        $results = '';
        //$address = "<button type='button' class='btn btn-wide btn-o btn-primary'id='edit-shenq_' data-id='$addressID'>$address</button>";
        $edit = "<button type='button' class='btn btn-small btn-o btn-primary'id='edit-shenq' data-id='$addressID'>Edit</button>";

        $splitedAddress = explode(',', $address);
        $marz = trim($splitedAddress[0]);
        $city = trim($splitedAddress[1]);
        $region = trim($splitedAddress[2]);
        $street = trim($splitedAddress[3]);
        $subStreet = trim($splitedAddress[4]);
        $shenq = trim($splitedAddress[5]);

        if($shenq == null){
            swap($subStreet, $shenq);
        }



        //$address = "<a href='edit_shenq.php?id=$addressID'>$address</a>";

        $query = "INSERT INTO shenq(address_id,
            address_string,
            city,
            region,
            street,
            sub_street,
            shenq,
            mutqeri_qanak,
            harkeri_tiv,
            bnakaranneri_tiv,
            tup,
            comments,
            edit) VALUES (
            :address_id,
            :address_string,
            :city,
            :region,
            :street,
            :sub_street,
            :shenq,
            :mutq,
            :hark,
            :bnakaran,
            :tup,
            :comments,
            :edit)";

        $stmt = $db->prepare($query);

        $stmt->bindParam(':address_id', $addressID, PDO::PARAM_STR);
        $stmt->bindParam(':address_string', $marz, PDO::PARAM_STR);
        $stmt->bindParam(':city', $city, PDO::PARAM_STR);
        $stmt->bindParam(':region', $region, PDO::PARAM_STR);
        $stmt->bindParam(':street', $street, PDO::PARAM_STR);
        $stmt->bindParam(':sub_street', $subStreet, PDO::PARAM_STR);
        $stmt->bindParam(':shenq', $shenq, PDO::PARAM_STR);

        $stmt->bindParam(':mutq', $mutq, PDO::PARAM_INT);
// use PARAM_STR although a number
        $stmt->bindParam(':hark', $hark, PDO::PARAM_INT);
        $stmt->bindParam(':bnakaran', $bnakaran, PDO::PARAM_STR);
        $stmt->bindParam(':tup', $tup, PDO::PARAM_STR);
        $stmt->bindParam(':comments', $comments, PDO::PARAM_STR);
        $stmt->bindParam(':edit', $edit, PDO::PARAM_STR);

        $stmt->execute();

        echo "Շենքի տվյալներն հաջողությամբ ավելացված են:";
    } catch (PDOException $e) {

        error_log("Error in " . $e->getFile() . " on line " . $e->getLine() . ": " . $e->getMessage());
        echo "Տեղի է ունեցել սխալ";
        return false;
    } catch (ErrorException $e) {
        echo "Տեղի է ունեցել սխալ";
        return false;
    }
}



function UpdateShenqData($addressID, $mutq, $hark, $bnakaran, $tup, $comments){
    try {
        $db = pdoConnect();
        $results = '';
        //$address = "<button type='button' class='btn btn-wide btn-o btn-primary'id='edit-shenq' data-id='$addressID'>$address</button>";
        //$address = "<a href='edit_shenq.php?id=$addressID'>$address</a>";

        $query = "UPDATE shenq SET mutqeri_qanak = :mutq,
            harkeri_tiv = :hark,
            bnakaranneri_tiv = :bnakaran,
            tup = :tup,
            comments = :comments
            WHERE address_id = :address_id";

        $stmt = $db->prepare($query);

        $stmt->bindParam(':address_id', $addressID, PDO::PARAM_INT);
        $stmt->bindParam(':mutq', $mutq, PDO::PARAM_INT);
// use PARAM_STR although a number
        $stmt->bindParam(':hark', $hark, PDO::PARAM_INT);
        $stmt->bindParam(':bnakaran', $bnakaran, PDO::PARAM_STR);
        $stmt->bindParam(':tup', $tup, PDO::PARAM_STR);
        $stmt->bindParam(':comments', $comments, PDO::PARAM_STR);

        $stmt->execute();

        echo "Շենքի տվյալներն հաջողությամբ խմբագրված են:";
    } catch (PDOException $e) {

        error_log("Error in " . $e->getFile() . " on line " . $e->getLine() . ": " . $e->getMessage());
        echo "Տեղի է ունեցել սխալ";
        return false;
    } catch (ErrorException $e) {
        echo "Տեղի է ունեցել սխալ";
        return false;
    }
}


function GetShenqData($addressID){
    try {
        $db = pdoConnect();
        $result = array();
        //$address = "<button type='button' class='btn btn-wide btn-o btn-primary'id='edit-shenq' data-id='$addressID'>$address</button>";
        //$address = "<a href='edit_shenq.php?id=$addressID'>$address</a>";

        $statement = $db->prepare("select * from shenq where address_id = :name");
        $statement->execute(array(':name' => "$addressID"));
        //echo $query;
        $result = $statement->fetchAll();
        //print_r($result);

        echo json_encode($result);
    } catch (PDOException $e) {

        error_log("Error in " . $e->getFile() . " on line " . $e->getLine() . ": " . $e->getMessage());
        echo "Տեղի է ունեցել սխալ";
        return false;
    } catch (ErrorException $e) {
        echo "Տեղի է ունեցել սխալ";
        return false;
    }
}



function GetAptData($addressID, $id){
    try {
        $db = pdoConnect();
        $result = array();
        //$address = "<button type='button' class='btn btn-wide btn-o btn-primary'id='edit-shenq' data-id='$addressID'>$address</button>";
        //$address = "<a href='edit_shenq.php?id=$addressID'>$address</a>";

        $statement = $db->prepare("select * from apt where address_id = :name and apt_hamar = :id");
        $statement->execute(array(':name' => "$addressID",
                                  ':id' => "$id"));
        //echo $query;
        $result = $statement->fetchAll();
        //print_r($result);

        echo json_encode($result);
    } catch (PDOException $e) {

        error_log("Error in " . $e->getFile() . " on line " . $e->getLine() . ": " . $e->getMessage());
        echo "Տեղի է ունեցել սխալ";
        return false;
    } catch (ErrorException $e) {
        echo "Տեղի է ունեցել սխալ";
        return false;
    }
}

function GetAllEmployees(){
    try {
        $db = pdoConnect();
        $emplolist = "";
        $result = array();
        //$address = "<button type='button' class='btn btn-wide btn-o btn-primary'id='edit-shenq' data-id='$addressID'>$address</button>";
        //$address = "<a href='edit_shenq.php?id=$addressID'>$address</a>";

        $statement = $db->prepare("select * from login_users where restricted = 0");
        $statement->execute();

        //echo $query;
        $statement->setFetchMode(PDO::FETCH_ASSOC);
        //print_r($statement->fetch());
        while ($r = $statement->fetch()) {
            $emplolist .= "<option value='$r[user_id]'>$r[name]</option>";
            //$emplolist .= sprintf('%s <br/>', $r['name']);
        }

        echo $emplolist;
    } catch (PDOException $e) {

        error_log("Error in " . $e->getFile() . " on line " . $e->getLine() . ": " . $e->getMessage());
        echo "Տեղի է ունեցել սխալ";
        return false;
    } catch (ErrorException $e) {
        echo "Տեղի է ունեցել սխալ";
        return false;
    }
}



function AttacheEmployees($addressID, $e_list){
    try {
        $db = pdoConnect();
        //$results = '';
        //$address = "<button type='button' class='btn btn-wide btn-o btn-primary'id='edit-shenq' data-id='$addressID'>$address</button>";
        //$address = "<a href='edit_shenq.php?id=$addressID'>$address</a>";

        $query = "INSERT INTO user_address_match(address_id,
            users,
            attache_date) VALUES (
            :address_id,
            :users,
            sysdate())";

        $stmt = $db->prepare($query);

        $stmt->bindParam(':address_id', $addressID, PDO::PARAM_INT);
        $stmt->bindParam(':users', $e_list, PDO::PARAM_STR);

        $stmt->execute();

        echo "Նշանակումը կատարված է:";
    } catch (PDOException $e) {

        error_log("Error in " . $e->getFile() . " on line " . $e->getLine() . ": " . $e->getMessage());
        echo "Տեղի է ունեցել սխալ";
        return false;
    } catch (ErrorException $e) {
        echo "Տեղի է ունեցել սխալ";
        return false;
    }
}

function swap(&$x,&$y) {
    $tmp=$x;
    $x=$y;
    $y=$tmp;
}