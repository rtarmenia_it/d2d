<?php

/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 * Easy set variables
 */

// DB table to use
$table = 'apt';

// Table's primary key
$primaryKey = 'id';

// Array of database columns which should be read and sent back to DataTables.
// The `db` parameter represents the column name in the database, while the `dt`
// parameter represents the DataTables column identifier. In this case simple
// indexes
$columns = array(
    array( 'db' => 'edit', 'dt' => 0 ), //address_id
    array( 'db' => 'address_str',  'dt' => 1 ),
    array( 'db' => 'city',  'dt' => 2 ),
    array( 'db' => 'region',  'dt' => 3 ),
    array( 'db' => 'street',  'dt' => 4 ),
    array( 'db' => 'sub_street',  'dt' => 5 ),
    array( 'db' => 'shenq',  'dt' => 6 ),
    array( 'db' => 'apt_hamar',   'dt' => 7 ),
    array( 'db' => 'apt_bnakecvac',     'dt' => 8 ),
    array( 'db' => 'inet_operator',     'dt' => 9 ),
    array( 'db' => 'sak_patet',     'dt' => 10 ),
    array( 'db' => 'sakagin',     'dt' => 11 ),
    array( 'db' => 'contract_end_date',     'dt' => 12 ),
    array( 'db' => 'ayl_nshumner',     'dt' => 13 )
);

// SQL server connection information
$sql_details = array(
    'user' => 'sevada',
    'pass' => 'AnkappasS123',
    'db'   => 'd2d',
    'host' => '192.168.1.176'
);


/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 * If you just want to use the basic configuration for DataTables with PHP
 * server-side, there is no need to edit below this line.
 */

require( 'ssp.class.php' );

echo json_encode(
    SSP::simple( $_GET, $sql_details, $table, $primaryKey, $columns )
);