<?php
/*

UserFrosting Version: 0.2.1 (beta)
By Alex Weissman
Copyright (c) 2014

Based on the UserCake user management system, v2.0.2.
Copyright (c) 2009-2012

UserFrosting, like UserCake, is 100% free and open-source.

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the 'Software'), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:
The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED 'AS IS', WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.

*/
error_reporting(E_ALL);
ini_set('display_errors', 'Off');

// All SQL queries use OCI8
function oracleConnect(){
$conn = oci_connect('readonly', 'readonly', 'gnc-orange-bill.rtarmenia.am:1521/RT2', 'AL32UTF8');
if (!$conn) {
    $e = oci_error();
    trigger_error(htmlentities($e['message'], ENT_QUOTES), E_USER_ERROR);
}
return $conn;
}


function oracleConnectCanChange(){
    $conn = oci_connect('OS_BILL_CAN_CHANGE', 'X9uDufNekk', 'gnc-orange-bill.rtarmenia.am:1521/RT2', 'AL32UTF8');
    if (!$conn) {
        $e = oci_error();
        trigger_error(htmlentities($e['message'], ENT_QUOTES), E_USER_ERROR);
    }
    else{
        $cursor=OCIParse($conn,
            "ALTER SESSION SET NLS_DATE_FORMAT='DD.MM.YYYY'");
        OCIExecute($cursor);
        OCIFreeCursor($cursor);

    }
    return $conn;
}


function oracleConnectRTOSS(){
$conn = oci_connect('OS_EQM_CAN_CHANGE', 'Deey5lahgh', 'eqm-orange-gnc.rtarmenia.am:1521/RTOSS', 'AL32UTF8');
    if (!$conn) {
        $e = oci_error();
        trigger_error(htmlentities($e['message'], ENT_QUOTES), E_USER_ERROR);
    }
    else{
        $cursor=OCIParse($conn,
            "ALTER SESSION SET NLS_DATE_FORMAT='DD.MM.YYYY'");
        OCIExecute($cursor);
        OCIFreeCursor($cursor);

    }
    return $conn;
}